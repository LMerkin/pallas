(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*              Extraction of individial time series from DE423              *)
(*===========================================================================*)
value layout =
[|
	(  3, 14, 3, 4);	(*  0: Mercury		*)
	(171, 10, 3, 2);	(*  1: Venus			*)
	(231, 13, 3, 2);	(*  2: EMB				*)
	(309, 11, 3, 1);	(*  3: Mars				*)
	(342,  8, 3, 1);	(*  4: Jupiter		*)
	(366,  7, 3, 1);	(*  5: Saturn			*)
	(387,  6, 3, 1);	(*  6: Uranus			*)
	(405,  6, 3, 1);	(*  7: Neptune		*)
	(423,  6, 3, 1);	(*  8: Pluto			*)
	(441, 13, 3, 8);	(*  9: Moon (GEO)	*)
	(753, 11, 3, 2);	(* 10: Sun				*)
	(819, 10, 2, 4);	(* 11: Nutations  *)
	(899, 10, 3, 4)		(* 12: Librations *)
|];
assert (Array.length layout = 13);

if (Array.length Sys.argv) <> 3
then
do{
	prerr_endline "PARAMETERS: <DE423_FILE> <OBJECT_# (1..13)>";
	exit 1
}
else ();

value de423_file = Sys.argv.(1);
value obj_no     = int_of_string Sys.argv.(2);

if obj_no < 0 || obj_no > 12
then
do{
	prerr_endline "ERROR: Invalid Object#, must be in 0..12";
	exit 1
}
else ();

(* Get the Layout:           *)
value    (obj_start, n_coeffs,  n_coords,  n_sets)  = layout.(obj_no);
value  set_size = n_coeffs * n_coords;
value  obj_size = n_sets   * set_size;
value  obj_off  = obj_start - 1;	(* Because "obj_start" is 1-based		*)

(* Call-Back on DE423 Lines: *)

value mjd_base    = 2400000.5;
type  de423_state =
{
	m_lstart: float;			(* Start time of the last (or curr) set       	 *)
	m_lstep : float;			(* Time step (span) of the last or curr set   	 *)
	m_lend  : float;			(* End time of the last set      								 *)
	m_roff  : int;				(* Offset of this LINE from the RECORD beginning *)
	m_scount: mutable int	(* Count  of time steps (i.e. sets)           	 *)
};

value proc_obj_fld: de423_state -> string -> int -> int =
fun state fld in_obj ->
	let in_coeffs = in_obj mod n_coeffs in
	let in_set    = in_obj mod set_size in
	do{
		(* BEGIN: *)
		if in_coeffs = 0
		then
		do{
			if in_set  = 0
			then
			do{
				let set_no = in_obj / set_size    in
				let t = state.m_lstart -. mjd_base +. (state.m_lstep *.
								(float_of_int set_no)) /. (float_of_int n_sets)
				in
				print_string (Printf.sprintf "\t\t\t{\t// %f\n" t)
			}
			else ();

			(* In any case, this is a beginning of a new component
				 (coeffs for a single co-ord):
			*)
			print_string "\t\t\t\t{ "
		}
		else ();

		(* MAIN BODY: *)
		(* Output the data item:    *)
		let afld = if fld.[0] <> '-' then " " ^ fld else fld in
		print_string afld;

		(* END: *)
		if in_coeffs <> n_coeffs - 1
		then
			(* An intermediate value. Install new line after each 2 values
				 for pretty formatting:
			*)
			print_string (if in_coeffs mod 2 = 1 then ",\n\t\t\t\t\t" else ", ")
		else
		do{
			(* End of a component (coeffs for a single co-ord): *)
			print_string " }";

			if in_set <> set_size - 1
			then
				print_string ",\n"
			else
				(* This is also the end of a co-ords block: *)
				print_string "\n\t\t\t},\n"
		};
		(* Return the update for count of sets (time steps): *)
		if in_set = 0 then 1 else 0
	};

value de423_line_action: list string -> de423_state -> de423_state =
fun flds state0 ->
	if (List.length flds) <> 3
	then
		(* Skip these flds -- they come before real data flds:  *)
		{ (state0) with m_roff = 0 }
	else
		(* Generic case: *)
		let state1 =
			if  state0.m_roff = 0
			then
				(* Beginning of a new record, check the time stamp: *)
				let t_start = float_of_string (List.hd  flds  ) in
				let t_end   = float_of_string (List.nth flds 1) in
				let t_step  = t_end -. t_start                  in
				(* Check the set span and contiguity if this is not the 1st set: *)
				if  state0.m_scount <> 0 &&
					(t_start <> state0.m_lend || t_step <> state0.m_lstep)
				then
				do{
					Printf.fprintf stderr
						"ERROR: LastStep=%f, LastEnd=%f, ThisStart=%f, ThisStep=%f\n"
						state0.m_lstep state0.m_lend t_start t_step;
					failwith "Invalid Data"
				}
				else
					{	(state0) with
							m_lstart = t_start;
							m_lstep  = t_step;
							m_lend   = t_end
					}
			else
				(* Inside (not at the beginning) of a record: *)
				state0
		in
		do{
			(* Process the data flds in this line: *)
			let i_from = if state1.m_roff = 0 then 2 else 0 in
			for i = i_from to 2
			do{
				(* Get the offset of the curr fld in the oject to be output (NB: the
					 curr fld may be outside that obj!):
				*)
				let in_obj = state1.m_roff + i - obj_off  in
				if  in_obj < 0 || in_obj >= obj_size
				then
					(* Outside of the object in question -- nothing to do:    *)
					()
				else
					(* Process the curr fld and possibly update the "scount": *)
					state1.m_scount := state1.m_scount +
						(proc_obj_fld state1 (List.nth flds i) in_obj)
			};
			(* Return the new state. NB: Only now "m_roff" is updated!    *)
			{ (state1) with m_roff = state1.m_roff + 3 }
		};


(* Process the input file: *)
value state0 =
{
	m_lstart = 0.0;
	m_lstep  = 0.0;
	m_lend   = 0.0;
	m_roff    = 0;
	m_scount = 0
};

value state1 = File_proc.run de423_file de423_line_action state0;

Printf.fprintf stderr "Total Sets: %d\n" state1.m_scount;
flush stderr;
flush stdout;

(* All done: *)
exit 0;

