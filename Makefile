all:
	cd DimTypes && make all
	cd AstroDyn && make all

install: all
	cd AstroDyn && make install

clean:
	rm -fr Objs/*

