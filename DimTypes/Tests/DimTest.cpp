// vim:ts=2
#include "DimTypes/Tests/AstroConsts.h"
#include <complex>
#include <cstdio>
#include <iostream>
using namespace std;

int main(int argc, char const* argv[])
{
	auto tonne = Mass_kg<complex<float>>(1000.0);
	auto tn1 = cbrt(tonne);
	cout << "cbrt  = " << Magnitude(tn1) << ' ' << DimStr(tn1) << endl;

	auto tn2 = - tn1;
	auto tn3 = abs(tn2);
	cout << "tn3   = " << Magnitude(tn3) << ' ' << DimStr(tn3) << endl;

	printf("c   = %.16e %s\n",
				 Magnitude(AstroConsts::c()),   DimStr(AstroConsts::c()).c_str());
	printf("GMS = %.16e %s\n",
				 Magnitude(AstroConsts::GMS()), DimStr(AstroConsts::GMS()).c_str());
	printf("GME = %.16e %s\n",
				 Magnitude(AstroConsts::GME()), DimStr(AstroConsts::GME()).c_str());

	auto GMS = To_Time_sec(To_Length_km(AstroConsts::GMS()));
	auto GME = To_Time_sec(To_Length_km(AstroConsts::GME()));

	printf("GMS = %.16e %s\n", Magnitude(GMS), DimStr(GMS).c_str());
	printf("GME = %.16e %s\n", Magnitude(GME), DimStr(GME).c_str());

	auto x = 10.0 * Length_m() / Time_sec();
	auto y = To_Length_km(To_Time_sec(x));
	auto z = 1.0 / y;
	auto dl  = y * z;
	auto cmx = AstroConsts::c() - x;

	printf("x   = %f %s\n", Magnitude(x), DimStr(x).c_str());
	printf("    = %f %s\n", Magnitude(y), DimStr(y).c_str());
	printf("1/x = %f %s\n", Magnitude(z), DimStr(z).c_str());
	printf("x/x = %f %s\n", Magnitude(dl), DimStr(dl).c_str());
	printf("c-x = %f %s\n", Magnitude(cmx), DimStr(cmx).c_str());
	return 0;
}
