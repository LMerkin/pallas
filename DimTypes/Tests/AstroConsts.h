// vim:ts=2
//===========================================================================//
//                             "AstroConsts.h":                              //
//       Fundamental Astronomical, Astrodynamical and Physical Consts        //
//                      (c) Dr Leonid Timochouk, 2011                        //
//===========================================================================//
// NB: The consts are made into inline functions in order to enable compile-
// time evaluation (and optimisation) of exprs containing such consts.
// XXX: This module is for testing purposes only!

#ifndef PALLAS_DIMTYPES_ASTROCONSTS_H
#define PALLAS_DIMTYPES_ASTROCONSTS_H

#include "DimTypes/Dimensions.h"

DECLARE_DIMS(Length, Time, Mass)

DECLARE_DIM_UNITS(Length, m, km, AU)
DECLARE_UNIT(Length, km, 1000.0)
DECLARE_UNIT(Length, AU, 1.495978706996262e+11)

DECLARE_DIM_UNITS(Time, sec, day)
DECLARE_UNIT(Time, day, 86400.0)

DECLARE_DIM_UNITS(Mass, kg)

namespace AstroConsts
{
	//=========================================================================//
	// Astronomical Constants (from DE423):                                    //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// Speed of light:                                                         //
	//-------------------------------------------------------------------------//
	template<typename T = double>
	inline decltype(Length_m<T>() / Time_sec<T>())
	c()
		{ return T(299792458.0) * Length_m<T>() / Time_sec<T>(); }

	//-------------------------------------------------------------------------//
	// Heliocentric Gravitational Constant:                                    //
	//-------------------------------------------------------------------------//
	template<typename T = double>
	inline decltype(Length_AU<T>().template pow<3>() /
									 Time_day<T>().template pow<2>())
	GMS()
	{
		return T(2.959122082855911e-4) *
					 Length_AU<T>().template pow<3>() / Time_day<T>().template pow<2>();
	}

	//-------------------------------------------------------------------------//
	// Earth / Moon Mass Ratio: dimension-less:                                //
	//-------------------------------------------------------------------------//
	template<typename T = double>
	constexpr inline T
	EMrat()
		{ return T(81.30056941599857); }

	//-------------------------------------------------------------------------//
	// Geocentric Gravitational Constant:                                      //
	//-------------------------------------------------------------------------//
	template<typename T = double>
	inline decltype(Length_AU<T>().template pow<3>() /
									 Time_day<T>().template pow<2>())
	GME()
	{
		return T(8.997011408268049e-10) / (T(1.0) + T(1.0) / EMrat<T>()) *
					 Length_AU<T>().template pow<3>() / Time_day<T>().template pow<2>();
	}
}

#endif	// PALLAS_DIMTYPES_ASTROCONSTS_H
