//===========================================================================//
//                              "FracPower.h":                               //
//               Compile-Time Reduction of Fractional Powers                 //
//===========================================================================//
// NB: This header is typically to be included from "Dimensions.h" but can also
// be used stand-alone:

#ifndef PALLAS_DIMTYPES_FRACPOWER_H
#define PALLAS_DIMTYPES_FRACPOWER_H

#include <cstdlib>
#include <cmath>
#include <complex>

namespace DimH
{
	using namespace std;

	//=========================================================================//
	// Overloaded / templated functions on representation types:               //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// "SqRt":                                                                 //
	//-------------------------------------------------------------------------//
	inline float SqRt(float x)
		{ return ::sqrtf(x); }

	inline double SqRt(double x)
		{ return ::sqrt(x);  }

	inline long double SqRt(long double x)
		{ return ::sqrtl(x); }

	template<typename T>
	inline complex<T> SqRt(complex<T> const& z)
		{ return sqrt(z);    }

	//-------------------------------------------------------------------------//
	// "CbRt":                                                                 //
	//-------------------------------------------------------------------------//
	inline float CbRt(float x)
		{ return ::cbrtf(x); }

	inline double CbRt(double x)
		{ return ::cbrt(x);  }

	inline long double CbRt(long double x)
		{ return ::cbrtl(x); }

	template<typename T>
	inline T CbRt(T x)
		{ return pow(x, T(1.0)/T(3.0)); }

	// NB: There is no built-in "CbRt" for "complex" types

	//-------------------------------------------------------------------------//
	// "Abs":                                                                  //
	//-------------------------------------------------------------------------//
	inline float Abs(float x)
		{ return ::fabsf(x); }

	inline double Abs(double x)
		{ return ::fabs(x);  }

	inline long double Abs(long double x)
		{ return ::fabsl(x); }

	// NB: For "complex" types, the result of "Abs" is lifted back to "complex":
	template<typename T>
	inline complex<T> Abs(complex<T> const& z)
		{ return complex<T>(abs(z)); }

	//-------------------------------------------------------------------------//
	// "Floor":                                                                //
	//-------------------------------------------------------------------------//
	inline float Floor(float x)
		{ return ::floorf(x); }

	inline double Floor(double x)
		{ return ::floor(x);  }

	inline long double Floor(long double x)
		{ return ::floorl(x); }

	// NB: For "complex" types, "Floor" is applied to both Re and Im:
	template<typename T>
	inline complex<T> Floor(complex<T> const& z)
		{ return complex<T>(Floor(z.real()), Floor(z.imag())); }

	//-------------------------------------------------------------------------//
	// "Ceil":                                                                 //
	//-------------------------------------------------------------------------//
	inline float Ceil(float x)
		{ return ::ceilf(x); }

	inline double Ceil(double x)
		{ return ::ceil(x);  }

	inline long double Ceil(long double x)
		{ return ::ceill(x); }

	// NB: For "complex" types, "Ceil" is applied to both Re and Im:
	template<typename T>
	inline complex<T> Ceil(complex<T> const& z)
		{ return complex<T>(Ceil(z.real()), Ceil(z.imag())); }

	//-------------------------------------------------------------------------//
	// "Round":                                                                //
	//-------------------------------------------------------------------------//
	inline float Round(float x)
		{ return ::roundf(x); }

	inline double Round(double x)
		{ return ::round(x);  }

	inline long double Round(long double x)
		{ return ::roundl(x); }

	// NB: For "complex" types, "Round" is applied to both Re and Im:
	template<typename T>
	inline complex<T> Round(complex<T> const& z)
		{ return complex<T>(Round(z.real()), Round(z.imag())); }

	//=========================================================================//
	// Compile-Time GCD and Normalisation Functions:                           //
	//=========================================================================//
	constexpr int GCDrec(int p, int q)
	{
		// 0 <= p <= q
		return (p == 0) ? q : GCDrec(q % p, p);
	}

	constexpr int GCD(int m, int n)
	{
		return abs(m) <= abs(n)
						? GCDrec(abs(m), abs(n))
						: GCDrec(abs(n), abs(m));
	}

	constexpr int NormaliseNumer(int m, int n)
	{
		// NB: GCD is always >= 0. Compile-time error occurs if GCD == 0 (which can
		// happen only if m == n == 0):
		return (n > 0)
						?   (m / GCD(m, n))
						: - (m / GCD(m, n));
	}

	constexpr int NormaliseDenom(int m, int n)
		{ return abs(n) / GCD(m, n); }

	//=========================================================================//
	// Power Templated Functions:                                              //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// Power Expr with a Natural Degree:                                       //
	//-------------------------------------------------------------------------//
	// The Even Positive case:
	template<typename T, int M, bool IsEven = (M % 2 == 0)>
	struct NatPower
	{
		static_assert((M >= 2) && IsEven, "Invalid degree in NatPower");
		static T res(T base)
		{
			T halfPow = NatPower<T, M/2>::res(base);
			return halfPow * halfPow;
		}
	};

	// The Odd case:
	template<typename T, int M>
	struct NatPower<T, M, false>
	{
		static_assert(M >= 1, "Invalid degree in NatPower");
		static T res(T base)
		{
			T halfPow = NatPower<T, M/2>::res(base);
			return base * halfPow * halfPow;
		}
	};

	// M==1: This case is not really necessary, but is provided for efficiency:
	template<typename T>
	struct NatPower<T, 1, false>
	{
		static T res(T base)
			{ return base; }
	};

	// M==0: Trivial case:
	template<typename T>
	struct NatPower<T, 0, true>
	{
		static T res(T base)
			{ return T(1.0); }	// XXX: We do not check if base==0
	};

	//-------------------------------------------------------------------------//
	// Power Expr with a General Integral Degree:                              //
	//-------------------------------------------------------------------------//
	// Natural case:
	template<typename T, int M, bool IsNatural = (M >= 0)>
	struct IntPower
	{
		static_assert(IsNatural, "IntPower: Case match error");
		static T res(T base)
			{ return NatPower<T, M>::res(base); }
	};

	// The Negative Integral case:
	template<typename T, int M>
	struct IntPower<T, M, false>
	{
		static T res(T base)
			{ return T(1.0) / NatPower<T, -M>::res(base); }
	};

	//-------------------------------------------------------------------------//
	// "SqRtPower":                                                            //
	//-------------------------------------------------------------------------//
	// The degree is a (normalised) Rational; testing the denom "N" for being a
	// multiple of 2 -- if so, rewriting the power expr via "SqRt".
	//
	// Generic case: N != 1 and N is NOT a multiple of 2: use "pow":
	template<typename T, int M, int N, bool IsSqrt = (N % 2 == 0)>
	struct SqRtPower
	{
		static_assert(N >= 3 && !IsSqrt, "SqRtPower: Degree not normalised");
		static T res(T base)
			{ return pow(base, T(M) / T(N)); }
	};

	// The Integral degree case (N==1):
	template<typename T, int M>
	struct SqRtPower<T, M, 1, false>
	{
		static T res(T base)
			{ return IntPower<T, M>::res(base); }
	};

	// "Sqrt" case: NB: "N" is indeed a multiple of 2:
	template<typename T, int M, int N>
	struct SqRtPower<T, M, N, true>
	{
		static_assert(N >= 2 && N % 2 == 0, "SqRtPower: Case match error");
		static T res(T base)
			{ return SqRtPower<T, M, N/2>::res(SqRt(base)); }
	};

	//-------------------------------------------------------------------------//
	// "CbRtPower":                                                            //
	//-------------------------------------------------------------------------//
	// Similar to "SqRtPower"; testing "N" for being a multiple of 3,  and if so,
	// rewriting the power expr via "CbRt". However, "CbRt" is only available for
	// real types.
	//
	// Generic case: N is NOT a multiple of 3, or inappropritate type "T":   Fall
	// back to "SqRtPower":
	template<typename T, int M, int N, bool IsCbrt = (N % 3 == 0)>
	struct CbRtPower
	{
		static T res(T base)
			{ return SqRtPower<T, M, N>::res(base); }
	};

	// The following types allow us to use "CbRt":
	template<int M, int N>
	struct CbRtPower<float, M, N, true>
	{
		static_assert(N >= 3 && N % 3 == 0, "CbRtPower: Case match error");
		static float res(float base)
			{ return CbRtPower<float, M, N/3>::res(CbRt(base)); }
	};

	template<int M, int N>
	struct CbRtPower<double, M, N, true>
	{
		static_assert(N >= 3 && N % 3 == 0, "CbRtPower: Case match error");
		static double res(double base)
			{ return CbRtPower<double, M, N/3>::res(CbRt(base)); }
	};

	template<int M, int N>
	struct CbRtPower<long double, M, N, true>
	{
		static_assert(N >= 3 && N % 3 == 0, "CbRtPower: Case match error");
		static long double res(long double base)
			{ return CbRtPower<long double, M, N/3>::res(CbRt(base)); }
	};

	//-------------------------------------------------------------------------//
	// Power Expr with a General Fractional Degree:                            //
	//-------------------------------------------------------------------------//
	// NB: This is a template function, not a struct -- it has no partial specs.
	// First attempt "CbRtPower", then it gets reduced further:
	template<int M, int N, typename T>
	inline T FracPower(T base)
	{
		static_assert(N != 0, "Zero denom in fractional degree");
		return CbRtPower<T, NormaliseNumer(M,N), NormaliseDenom(M,N)>::res(base);
	}
}

#endif	// PALLAS_DIMTYPES_FRACPOWER_H
