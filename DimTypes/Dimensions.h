// vim:ts=2
//===========================================================================//
//                               "Dimensions.h":                             //
//          Types for Physical Dimensions and Dimensioned Quantities         //
//                       (c) Dr Leonid Timochouk, 2011                       //
//===========================================================================//
#ifndef PALLAS_DIMTYPES_DIMENSIONS_H
#define PALLAS_DIMTYPES_DIMENSIONS_H

#include "DimTypes/FracPower.h"
#include "DimTypes/Encodings.h"
#include <utility>
#include <string>

//===========================================================================//
// "DimQ": The External Dimensioned Type:                                    //
//===========================================================================//
// "DimQ" is actually the representation type "T" statically parameterised by
// the Dimension Exponent "E" and the vector of units "U":
//
template<unsigned long E, unsigned long U, typename T = double>
class DimQ
{
private:
	T m_val;	// The actual value (magnitude)

public:
	//-------------------------------------------------------------------------//
	// Ctors and Access:                                                       //
	//-------------------------------------------------------------------------//
	// Ctor: lifting a value into "DimQ" (by default, 0):
	explicit DimQ(T const& val = T(0.0)): m_val(val) {}

	static DimQ Zero() 	  { return DimQ(); }
	// NB: Copy ctor and assignment are auto-generated

	// Change of representation: S -> T (dimensions and units are unchanged):
	template<typename S>
	explicit DimQ(DimQ<E,U,S> const& right)
		: DimQ(T(right.Magnitude())) {}

	// The dimensioned unit for the given dimensioned quantity. NB: This method
	// is not a ctor: it takes an existing "DimQ" and returns a similar one but
	// with the unitary magnitude. Postfix and prefix forms are provided:
	DimQ UnitOf() const	{ return DimQ(T(1.0)); }

	friend DimQ UnitOf(DimQ const& dimq) { dimq.UnitOf(); }

	// Getting the exponent code (this is primarily for testing) -- postfix and
	// prefix forms:
	unsigned long GetDimsCode() const  { return E; }

	friend unsigned long GetDimsCode(DimQ const& dimq)
		{ return dimq.GetDimsCode();  }

	// Getting the Units code (also for testing) -- postfix and prefix forms:
	unsigned long GetUnitsCode() const { return U; }

	friend unsigned long GetUnitsCode(DimQ const& dimq)
		{ return dimq.GetUnitsCode(); }

	// The magnitude of the quantity (i.e. its value expressed in the corresp
	// dimensioned units). The magnitude is by itself dimensionless.
	// XXX: direct access to the dimension-less magnitude  may seem  to be an
	// unsafe feature but it could be emulated anyway (by dividing the qty by
	// its unit), so we provide direct access to it for efficiency.   Postfix
	// and prefix forms are provided:
	T Magnitude() const
		{ return m_val; }

	friend T Magnitude(DimQ const& dimq)
		{ return dimq.m_val; }

	// The following function converts "DimQ" to the rep type ONLY if it is di-
	// mension-less:
	T DimLess() const
	{
		static_assert(E == 0, "Must be a DimLess qty");
		return m_val;
	}

	friend T DimLess(DimQ const& dimq)
		{ return dimq.DimLess(); }

	//-------------------------------------------------------------------------//
	// Arithmetic Operations -- Dimensions Unchanged:                          //
	//-------------------------------------------------------------------------//
	// Addition of "DimQ"s:
	// This is only possible if the dimensions and rep are same, and the units
	// can be unified (i.e. same units for non-0 exponents), in which case the
	// 1st arg's units can still be used:
	//
	template<unsigned long V>
	DimQ operator+(DimQ<E,V,T> const& right) const
	{
		static_assert(DimH::UnitsOK(E,U,V), "ERROR: Units do not unify");
		return DimQ(m_val + right.m_val);
	}

	template<unsigned long V>
	DimQ& operator+=(DimQ<E,V,T> const& right)
	{
		static_assert(DimH::UnitsOK(E,U,V), "ERROR: Units do not unify");
		m_val += right.m_val;
		return *this;
	}

	// Subtraction of "DimQs":
	// Same constraints as for the addition:
	template<unsigned long V>
	DimQ operator-(DimQ<E,V,T> const& right) const
	{
		static_assert(DimH::UnitsOK(E,U,V), "ERROR: Units do not unify");
		return DimQ(m_val - right.m_val);
	}

	template<unsigned long V>
	DimQ& operator-=(DimQ<E,V,T> const& right)
	{
		static_assert(DimH::UnitsOK(E,U,V), "ERROR: Units do not unify");
		m_val -= right.m_val;
		return *this;
	}

	// Multiplication by a Dimension-Less Factor:
	// NB: the following method can be used to create new dimensioned values
	// from the fundamental units. XXX: when used this way, it is slighly in-
	// efficient because of multiplication by 1 and extra copying;   but the
	// impact of that should be negligible:
	DimQ operator*(T const& right) const
		{ return DimQ(m_val * right); }

	DimQ& operator*=(T const& right)
	{
		m_val *= right;
		return *this;
	}

	friend DimQ operator*(T const& left, DimQ const& right)
		{ return DimQ(left * right.m_val); }

	// In particular, unary negation:
	DimQ operator-() const
		{ return DimQ(-m_val); }

	// Division by a Dimension-Less Factor:
	DimQ operator/(T const& right) const
		{ return DimQ(m_val / right); }

	DimQ& operator/=(T const& right)
	{
		m_val /= right;
		return *this;
	}

	// "abs"; "floor", "ceil", "round" (if supported by the rep type "T"):
	//
#	define DIMQ_UNARY_SHORTCUT(UFName, ImplName) \
	DimQ UFName() const \
		{ return DimQ(DimH::ImplName(m_val)); } \
	\
	friend DimQ UFName(DimQ const& right) \
		{ return right.UFName(); }

	DIMQ_UNARY_SHORTCUT(abs,   Abs)
	DIMQ_UNARY_SHORTCUT(floor, Floor)
	DIMQ_UNARY_SHORTCUT(ceil,  Ceil)
	DIMQ_UNARY_SHORTCUT(round, Round)

	//-------------------------------------------------------------------------//
	// Arithmetic operations which result in dimensions change:                //
	//-------------------------------------------------------------------------//
	// Multiplication of "DimQ"s:
	// Dimension exponents are added up; units must unify;  afterwards, units of
	// zero-exp dims are reset, otherwise false type-checking failures may occur:
	//
	template<unsigned long F, unsigned long V>
	DimQ<DimH::AddExp(E,F),
			 DimH::CleanUpUnits(DimH::AddExp(E,F), DimH::UnifyUnits<E,F,U,V>()), T>
	operator*(DimQ<F,V,T> const& right) const
	{
		return DimQ<DimH::AddExp(E,F),
								DimH::CleanUpUnits(DimH::AddExp(E,F),
																	 DimH::UnifyUnits<E,F,U,V>()), T>
					 (m_val * right.Magnitude());
	}

	// Division of "DimQ"s:
	// Dimension exponents are subtracted. Same treatment of units as for mult:
	template<unsigned long F, unsigned long V>
	DimQ<DimH::SubExp(E,F),
			 DimH::CleanUpUnits(DimH::SubExp(E,F), DimH::UnifyUnits<E,F,U,V>()), T>
	operator/(DimQ<F,V,T> const& right) const
	{
		return DimQ<DimH::SubExp(E,F),
								DimH::CleanUpUnits(DimH::SubExp(E,F),
																	 DimH::UnifyUnits<E,F,U,V>()), T>
					 (m_val / right.Magnitude());
	}

	friend DimQ<DimH::SubExp(0UL,E),U,T>
	operator/(T const& left, DimQ const& right)
		{ return DimQ<DimH::SubExp(0UL,E),U,T>(left / right.m_val); }

	// Integral and Rational Powers -- the power must be given by constant exprs
	// so it is analysable at compile time:
	// NB: for applying these methods from outside "DimQ", explicit "template pow"	// expr is required, otherwise the template is not syntactically recognised!
	// NB: "CleanUpUnits" is required in case if M==0:
	//
	template<int M, int N = 1>
	DimQ<DimH::DivExp(DimH::MultExp(E,M),N),
			 DimH::CleanUpUnits(DimH::DivExp(DimH::MultExp(E,M),N), U), T>
	pow() const
	{
		// Multiples of "PMod" (including 0) are uninvertible in the curr rep:
		static_assert(N % DimH::IPMod != 0, "Invalid denom in frac power");

		return DimQ<DimH::DivExp(DimH::MultExp(E,M),N),
								DimH::CleanUpUnits(DimH::DivExp(DimH::MultExp(E,M),N), U), T>
					(DimH::FracPower<M, N, T>(m_val));
	}

	template<int M, int N = 1>
	friend DimQ<DimH::DivExp(DimH::MultExp(E,M),N),
							DimH::CleanUpUnits(DimH::DivExp(DimH::MultExp(E,M),N), U), T>
	pow(DimQ const& right)
		{ return right.pow<M,N>(); }

	// Shortcuts: "sqrt" and "cbrt". Here M==1, so "CleanUpUnits" is not requird:
	//
#	define DIMQ_ROOT_SHORTCUT(NameRt,Denom) \
	DimQ<DimH::DivExp(E,Denom),U,T>  NameRt() const \
		{ return pow<1,Denom>(); } \
	\
	friend DimQ<DimH::DivExp(E,Denom),U,T>  NameRt(DimQ const& right) \
		{ return right.NameRt(); }

	DIMQ_ROOT_SHORTCUT(sqrt,2)
	DIMQ_ROOT_SHORTCUT(cbrt,3)

	//-------------------------------------------------------------------------//
	// Comparison operators:                                                   //
	//-------------------------------------------------------------------------//
	// Require same representation, same dimensions and unifyable units.
	// NB: inequalities would result in a compile-time error if not implemented
	// for the corresp underlying type (e.g. complex) (but only at use point!).
	//
	// NB: interestingly, a space is allowed between e.g. "operator" and "=="!
	//
#	define DIMQ_CMP(Op) \
	template<unsigned long V> \
	bool operator Op(DimQ<E,V,T> const& right) const \
	{ \
		static_assert(DimH::UnitsOK(E,U,V), "ERROR: Units do not unify"); \
		return m_val Op right.m_val; \
	}
	DIMQ_CMP(==)
	DIMQ_CMP(!=)
	DIMQ_CMP(>)
	DIMQ_CMP(>=)
	DIMQ_CMP(<)
	DIMQ_CMP(<=)

	bool IsZero() const												{ return m_val == T(0.0);	 	 }
	friend bool IsZero(DimQ const& right) 		{ return right.IsZero();	 	 }

	bool IsFinite() const											{ return isfinite(m_val);		 }
	friend bool IsFinite(DimQ const& right)		{ return right.IsFinite();	 }

	// NB: The following methods will not compile if the field "T" is not
	// ordered (e.g. complex numbers):
	//
	bool IsNegative() const										{ return m_val <  T(0.0);  	 }
	friend bool IsNegative(DimQ const& right)	{ return right.IsNegative(); }

	bool IsPositive() const										{ return m_val >  T(0.0);		 }
	friend bool IsPositive(DimQ const& right)	{ return right.IsPositive(); }

	//-------------------------------------------------------------------------//
	// Unit Conversion:                                                        //
	//-------------------------------------------------------------------------//
	// Again, explicit "template" keyword is required to invoke these methods from
	// outside the "DimQ" class:
	// NB: If this "DimQ" has 0 exp wrt "Dim", then no conversion is performed and
	// ALL unused units are re-set:
	//
	template<unsigned int Dim, unsigned int NewUnit>
	DimQ<E, DimH::CleanUpUnits(E, DimH::SetUnit(U,Dim,NewUnit)), T>
	ToUnit() const
	{
		return
			DimQ<E, DimH::CleanUpUnits(E, DimH::SetUnit(U,Dim,NewUnit)), T>
				(m_val *
				 DimH::FracPower
					<DimH::Numer(DimH::GetFld(E,Dim)),
					 DimH::Denom(DimH::GetFld(E,Dim)), T>
				 // OldScale / NewScale:
				 (DimH::UnitScale<Dim, DimH::GetFld(U,Dim), T>::val() /
					DimH::UnitScale<Dim, NewUnit, T>::val())
				);
	}

	template<unsigned int Dim, unsigned int NewUnit>
	friend DimQ<E, DimH::SetUnit(U,Dim,NewUnit), T> ToUnit(DimQ const& dimq)
		{ return dimq.ToUnit<Dim,NewUnit>();  }

	//-------------------------------------------------------------------------//
	// Stringification of the Dimension and Units:                             //
	//-------------------------------------------------------------------------//
	// NB: This does NOT include the magnitude because its format is type- and
	// user-specific:
	std::string DimStr() const
		{ return DimH::DimStr<E,U>(); }

	friend std::string DimStr(DimQ const& dimq)
		{ return dimq.DimStr();     }
};

//===========================================================================//
// Macros for User-Level Declarations of Dimensions and Units:               //
//===========================================================================//
// XXX: These macros must be invoked from the global namespace only!

// "DECLARE_DIMS":
// First macro to be invoked at the user level. Declares the enum type (called
// "Dims") of all dimensions to be used. This assigns encodings to dimensions:
//
#define DECLARE_DIMS(...) \
	enum class Dims { __VA_ARGS__ };

// "DECLARE_DIM_UNITS":
// Declaring all units of a given dimension and thus assigned per-dimension en-
// codings to them. Automatically invokes "DECLARE_UNIT" for the 1st (Fundamen-
// tal) unit, but for all other (aux) units listed here,   "DECLARE_UNIT" must
// subsequently be invoked explicitly:
//
#define DECLARE_DIM_UNITS(DimName, FundUnitName, ...)  \
	static_assert((unsigned int)(Dims::DimName) < DimH::NDims, "Too many dims"); \
	enum class DimName##Units { FundUnitName, __VA_ARGS__ }; \
	DECLARE_UNIT(DimName, FundUnitName, 1.0)

// "DECLARE_UNIT": 
// For a given unit, declares a factory function,   the stringifier specialsn,
// the scale (value in Fundamental Units) specialn, and a conversion funcs for
// arbitrary "DimQ"s into this Unit.
// NB: with the naming conventioned used, it is OK to have same-named units for
// different dims:
//
#define DECLARE_UNIT(DimName, UnitName, UnitVal) \
	static_assert((unsigned int)(DimName##Units::UnitName) <= DimH::PMod, \
								"Too many units for this dim"); \
	\
	template<typename T = double> \
	inline \
	DimQ<DimH::DimExp((unsigned int)(Dims::DimName)), \
			 DimH::MkUnit((unsigned int)(Dims::DimName), \
										(unsigned int)(DimName##Units::UnitName)), T> \
	DimName##_##UnitName(T const& val = T(1.0)) \
	{ \
		return DimQ<DimH::DimExp((unsigned int)(Dims::DimName)), \
								DimH::MkUnit((unsigned int)(Dims::DimName), \
														 (unsigned int)(DimName##Units::UnitName)), T> \
					 (val); \
	} \
	\
	namespace DimH \
	{ \
		template<> \
		struct UnitImage<(unsigned int)(Dims::DimName), \
										 (unsigned int)(DimName##Units::UnitName)> \
		{ \
			static char const* str() { return #UnitName; } \
		}; \
		\
		template<typename T> \
		struct UnitScale<(unsigned int)(Dims::DimName), \
										 (unsigned int)(DimName##Units::UnitName), T> \
		{ \
			static T val() { return T(UnitVal); }   \
		}; \
	} \
	\
	template<unsigned long E, unsigned long U, typename T>   \
	inline \
	DimQ<E, DimH::SetUnit(U, (unsigned int)(Dims::DimName), \
												 	 (unsigned int)(DimName##Units::UnitName)), T> \
	To_##DimName##_##UnitName(DimQ<E,U,T> const& dimq) \
	{ \
		return dimq.template ToUnit<(unsigned int)(Dims::DimName), \
																(unsigned int)(DimName##Units::UnitName)>(); \
	}

#endif	// PALLAS_DIMTYPES_DIMENSIONS_H
