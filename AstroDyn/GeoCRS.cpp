// vim:ts=2
//===========================================================================//
//                               "GeoCRS.cpp":                               //
//            GeoCentric Celestial Rectangular Reference System              //
//                      (c) Dr Leonid Timochouk, 2011                        //
//===========================================================================//
#include "AstroDyn/GeoCRS.h"
#include "AstroDyn/PoissonSOFA.h"
#include "AstroDyn/Earth_SOFA.h"
#include <stdexcept>
#include <cstdlib>

//===========================================================================//
// "EarthInHCRS" Switch:                                                     //
//===========================================================================//
// Position, Velocity and Acceleration of the Earth in the HCRS system;
// Time arg is TDB;
// the data structures are compiled separately.
//
namespace
{
	using namespace AstroDyn;
	using namespace std;

	template<bool WithVel, bool WithAcc>
	void EarthInHCRS(ImplType I, TDB tdb, LenAU r[3], VelAUD v[3], AccAUD2 a[3])
	{
		switch (I)
		{
		case ImplType::SOFA:
			ImplSOFA::PoissonCosSum<WithVel,WithAcc>
								(ImplSOFA::Sun2Earth, tdb, r, v, a);
			break;

		default:
			throw runtime_error("EarthInHCRS: Unsupported ImplType");
		}
	}
}

//===========================================================================//
// Implementation of Externally-Visible Functions:                           //
//===========================================================================//
namespace AstroDyn
{
	// NB: no template<> decls anymore here!

	//=========================================================================//
	// GCRS1:                                                                  //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// HCRS <-> GCRS1: Pos only:                                               //
	//-------------------------------------------------------------------------//
	// GCRS1<-  HCRS:
	GCRS1<SVType::P> ToGCRS1(HCRS<SVType::P> const& hcrs, ImplType I)
	{
		// Get the HCRS coords of the Earth:
		LenAU EarthPos[3];
		EarthInHCRS<false, false>(I, hcrs.m_tdb, EarthPos, NULL, NULL);

		// Convert time and compute the relative GeoCentric vector:
		return GCRS1<SVType::P>
		{
			ToTCG(ToTT(hcrs.m_tdb)),
			{
				hcrs.m_r.m_x - EarthPos[0],
				hcrs.m_r.m_y - EarthPos[1],
				hcrs.m_r.m_z - EarthPos[2]
			}
		};
	}

	// HCRS <-  GCRS1:
	HCRS<SVType::P> ToHCRS(GCRS1<SVType::P> const& gcrs1, ImplType I)
	{
		// Convert the time:
		TDB tdb = ToTDB(ToTT(gcrs1.m_tcg));

		// Get the HCRS coords of the Earth:
		LenAU EarthPos[3];
		EarthInHCRS<false, false>(I, tdb, EarthPos, NULL, NULL);

		// Convert time and compute the total HelioCentric vector:
		return HCRS<SVType::P>
		{
			tdb,
			{
				gcrs1.m_r.m_x + EarthPos[0],
				gcrs1.m_r.m_y + EarthPos[1],
				gcrs1.m_r.m_z + EarthPos[2]
			}
		};
	}

	//-------------------------------------------------------------------------//
	// HCRS <-> GCRS1: Pos and Vel:                                            //
	//-------------------------------------------------------------------------//
	// GCRS1<-  HCRS:
	GCRS1<SVType::PV> ToGCRS1(HCRS<SVType::PV> const& hcrs, ImplType I)
	{
		// Get the HCRS coords of the Earth:
		LenAU 	EarthPos[3];
		VelAUD	EarthVel[3];
		EarthInHCRS<true, false>(I, hcrs.m_tdb, EarthPos, EarthVel, NULL);

		// Convert time and compute the relative GeoCentric vector:
		return GCRS1<SVType::PV>
		{
			ToTCG(ToTT(hcrs.m_tdb)),
			{
				hcrs.m_r.m_x - EarthPos[0],
				hcrs.m_r.m_y - EarthPos[1],
				hcrs.m_r.m_z - EarthPos[2]
			},
			{
				hcrs.m_v.m_x - EarthVel[0],
				hcrs.m_v.m_y - EarthVel[1],
				hcrs.m_v.m_z - EarthVel[2]
			}
		};
	}

	// HCRS <-  GCRS1:
	HCRS<SVType::PV> ToHCRS(GCRS1<SVType::PV> const& gcrs1, ImplType I)
	{
		// Convert the time:
		TDB tdb = ToTDB(ToTT(gcrs1.m_tcg));

		// Get the HCRS coords of the Earth:
		LenAU		EarthPos[3];
		VelAUD	EarthVel[3];
		EarthInHCRS<true, false>(I, tdb, EarthPos, EarthVel, NULL);

		// Convert time and compute the total HelioCentric vector:
		return HCRS<SVType::PV>
		{
			tdb,
			{
				gcrs1.m_r.m_x + EarthPos[0],
				gcrs1.m_r.m_y + EarthPos[1],
				gcrs1.m_r.m_z + EarthPos[2]
			},
			{
				gcrs1.m_v.m_x + EarthVel[0],
				gcrs1.m_v.m_y + EarthVel[1],
				gcrs1.m_v.m_z + EarthVel[2]
			}
		};
	}

	//-------------------------------------------------------------------------//
	// HCRS <-> GCRS1: Pos, Vel and Acc:                                       //
	//-------------------------------------------------------------------------//
	// GCRS1<-  HCRS:
	GCRS1<SVType::PVA> ToGCRS1(HCRS<SVType::PVA> const& hcrs, ImplType I)
	{
		// Get the HCRS coords of the Earth:
		LenAU 	EarthPos[3];
		VelAUD	EarthVel[3];
		AccAUD2	EarthAcc[3];
		EarthInHCRS<true, true>(I, hcrs.m_tdb, EarthPos, EarthVel, EarthAcc);

		// Convert time and compute the relative GeoCentric vector:
		return GCRS1<SVType::PVA>
		{
			ToTCG(ToTT(hcrs.m_tdb)),
			{
				hcrs.m_r.m_x - EarthPos[0],
				hcrs.m_r.m_y - EarthPos[1],
				hcrs.m_r.m_z - EarthPos[2]
			},
			{
				hcrs.m_v.m_x - EarthVel[0],
				hcrs.m_v.m_y - EarthVel[1],
				hcrs.m_v.m_z - EarthVel[2]
			},
			{
				hcrs.m_a.m_x - EarthAcc[0],
				hcrs.m_a.m_y - EarthAcc[1],
				hcrs.m_a.m_z - EarthAcc[2]
			}
		};
	}

	// HCRS <-  GCRS1:
	HCRS<SVType::PVA> ToHCRS(GCRS1<SVType::PVA> const& gcrs1, ImplType I)
	{
		// Convert the time:
		TDB tdb = ToTDB(ToTT(gcrs1.m_tcg));

		// Get the HCRS coords of the Earth:
		LenAU		EarthPos[3];
		VelAUD	EarthVel[3];
		AccAUD2	EarthAcc[3];
		EarthInHCRS<true, true>(I, tdb, EarthPos, EarthVel, EarthAcc);

		// Convert time and compute the total HelioCentric vector:
		return HCRS<SVType::PVA>
		{
			tdb,
			{
				gcrs1.m_r.m_x + EarthPos[0],
				gcrs1.m_r.m_y + EarthPos[1],
				gcrs1.m_r.m_z + EarthPos[2]
			},
			{
				gcrs1.m_v.m_x + EarthVel[0],
				gcrs1.m_v.m_y + EarthVel[1],
				gcrs1.m_v.m_z + EarthVel[2]
			},
			{
				gcrs1.m_a.m_x + EarthAcc[0],
				gcrs1.m_a.m_y + EarthAcc[1],
				gcrs1.m_a.m_z + EarthAcc[2]
			}
		};
	}

	//=========================================================================//
	// GCRS2:                                                                  //
	//=========================================================================//
	// This only involves conversion of Units:
	//-------------------------------------------------------------------------//
	// GCRS1 <-> GCRS2: Pos only:                                              //
	//-------------------------------------------------------------------------//
	// GCRS2 <-  GCRS1:
	GCRS2<SVType::P> ToGCRS2(GCRS1<SVType::P> const& gcrs1)
	{
		return GCRS2<SVType::P>
		{
			gcrs1.m_tcg,
			{
				To_Length_km(gcrs1.m_r.m_x),
				To_Length_km(gcrs1.m_r.m_y),
				To_Length_km(gcrs1.m_r.m_z)
			}
		};
	}

	// GCRS1 <-  GCRS2:
	GCRS1<SVType::P> ToGCRS1(GCRS2<SVType::P> const& gcrs2)
	{
		return GCRS1<SVType::P>
		{
			gcrs2.m_tcg,
			{
				To_Length_AU(gcrs2.m_r.m_x),
				To_Length_AU(gcrs2.m_r.m_y),
				To_Length_AU(gcrs2.m_r.m_z)
			}
		};
	}

	//-------------------------------------------------------------------------//
	// GCRS1 <-> GCRS2: Pos and Vel:                                           //
	//-------------------------------------------------------------------------//
	// GCRS2 <-  GCRS1:
	GCRS2<SVType::PV> ToGCRS2(GCRS1<SVType::PV> const& gcrs1)
	{
		return GCRS2<SVType::PV>
		{
			gcrs1.m_tcg,
			{
				To_Length_km(gcrs1.m_r.m_x),
				To_Length_km(gcrs1.m_r.m_y),
				To_Length_km(gcrs1.m_r.m_z)
			},
			{
				To_Length_km(To_Time_sec(gcrs1.m_v.m_x)),
				To_Length_km(To_Time_sec(gcrs1.m_v.m_y)),
				To_Length_km(To_Time_sec(gcrs1.m_v.m_z))
			}
		};
	}

	// GCRS1 <-  GCRS2:
	GCRS1<SVType::PV> ToGCRS1(GCRS2<SVType::PV> const& gcrs2)
	{
		return GCRS1<SVType::PV>
		{
			gcrs2.m_tcg,
			{
				To_Length_AU(gcrs2.m_r.m_x),
				To_Length_AU(gcrs2.m_r.m_y),
				To_Length_AU(gcrs2.m_r.m_z)
			},
			{
				To_Length_AU(To_Time_day(gcrs2.m_v.m_x)),
				To_Length_AU(To_Time_day(gcrs2.m_v.m_y)),
				To_Length_AU(To_Time_day(gcrs2.m_v.m_z))
			}
		};
	}

	//-------------------------------------------------------------------------//
	// GCRS1 <-> GCRS2: Pos and Vel:                                           //
	//-------------------------------------------------------------------------//
	// GCRS2 <-  GCRS1:
	GCRS2<SVType::PVA> ToGCRS2(GCRS1<SVType::PVA> const& gcrs1)
	{
		return GCRS2<SVType::PVA>
		{
			gcrs1.m_tcg,
			{
				To_Length_km(gcrs1.m_r.m_x),
				To_Length_km(gcrs1.m_r.m_y),
				To_Length_km(gcrs1.m_r.m_z)
			},
			{
				To_Length_km(To_Time_sec(gcrs1.m_v.m_x)),
				To_Length_km(To_Time_sec(gcrs1.m_v.m_y)),
				To_Length_km(To_Time_sec(gcrs1.m_v.m_z))
			},
			{
				To_Length_km(To_Time_sec(gcrs1.m_a.m_x)),
				To_Length_km(To_Time_sec(gcrs1.m_a.m_y)),
				To_Length_km(To_Time_sec(gcrs1.m_a.m_z))
			}
		};
	}

	// GCRS1 <-  GCRS2:
	GCRS1<SVType::PVA> ToGCRS1(GCRS2<SVType::PVA> const& gcrs2)
	{
		return GCRS1<SVType::PVA>
		{
			gcrs2.m_tcg,
			{
				To_Length_AU(gcrs2.m_r.m_x),
				To_Length_AU(gcrs2.m_r.m_y),
				To_Length_AU(gcrs2.m_r.m_z)
			},
			{
				To_Length_AU(To_Time_day(gcrs2.m_v.m_x)),
				To_Length_AU(To_Time_day(gcrs2.m_v.m_y)),
				To_Length_AU(To_Time_day(gcrs2.m_v.m_z))
			},
			{
				To_Length_AU(To_Time_day(gcrs2.m_a.m_x)),
				To_Length_AU(To_Time_day(gcrs2.m_a.m_y)),
				To_Length_AU(To_Time_day(gcrs2.m_a.m_z))
			}
		};
	}
}
