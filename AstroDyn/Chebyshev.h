// vim:ts=2
//===========================================================================//
//                              "Chebyshev.h":                               //
//      Computation of Chebyshev polynomials, derivatives and series         //
//                       (c) Dr Leonid Timochouk, 2011                       //
//===========================================================================//
#ifndef  PALLAS_ASTRODYN_CHEBYSHEV_H
#define  PALLAS_ASTRODYN_CHEBYSHEV_H

namespace Chebyshev
{
	//-----------------//
	//  T(n,x):        //
	//-----------------//
	//  Value of the Chebyshev polynomial of order "n" (n >= 0) at "x":
	//
	double T(int n, double x, bool permissive = false);

	//-----------------//
	//  TT(n,x):       //
	//-----------------//
	//  Translated Chebyshev polynomaial, x in [0..1]: TT(n,x) = T(n,2x-1) :
	//
	inline double TT(int n, double x, bool permissive = false)
		{ return T(n, 2.0 * x - 1.0, permissive); }

	//-----------------//
	//  Ts(n,x) :      //
	//-----------------//
	//  T_0(x), ..., T_n(x) evaluated at the same point "x"; results
	//  stored in "res"; len(res) == n+1 (must have sufficient size):
	//
	void Ts(int n, double x, double* res, bool permissive = false);

	//-----------------//
	//  TTs(n,x):      //
	//-----------------//
	//  As Ts(n,x) above, but for Translated Chebyshev Polynomials:
	//
	inline void TTs(int n, double x, double* res, bool permissive = false)
	{
		Ts(n, 2.0 * x - 1.0, res, permissive);
	}

	//-----------------//
	//  Sum1T(n,a,x):  //
	//-----------------//
	//  Efficient evaluation of Sum'_{i=0}^n a_i T_i(x), len(a) == n+1:
	//
	double Sum1T(int n, double const* a, double x, bool permissive = false);

	//-----------------//
	//  Sum1TT(n,a,x): //
	//-----------------//
	//  As Sum1T(n,a,x) above, but for Translated Chebyshev Polynomials:
	//
	inline double Sum1TT
		(int n, double const* a, double x, bool permissive = false)
	{
		return Sum1T(n, a, 2.0 * x - 1.0, permissive);
	}

	//-----------------//
	//  DT(n,x):       //
	//-----------------//
	//  dT_n/dx at x, n >= 0:
	//
	double DT(int n, double x, bool permissive = false);

	//-----------------//
	// DTs(n, x):      //
	//-----------------//
	// dT_i/dx at x, for all i=0..n; res[0] is always set to 0;
	// pre-cond: len(res) == n+1:
	//
	void DTs(int n, double x, double* res, bool permissive = false);

	//-----------------//
	//  SumDT(n,a,x):  //
	//-----------------//
	//  Efficient evaluation of Sum_{i=1}^{n} a_i dT_i/dx, len(a) == n+1,
	//  a[0] is irrelevant, so in this case Sum' = Sum:
	//
	double SumDT(int n, double const* a, double x, bool permissive = false);

	//-----------------//
	//  DDT(n,x):      //
	//-----------------//
	//  d^2 T_n / dx^2 at "x", n >= 0:
	//
	double DDT(int n, double x, bool permissive = false);

	//-----------------//
	//  DDTpm1(n,x):   //
	//-----------------//
	//  Special case of the above: "x" must be +-1 :
	//
	double DDTpm1(int n, double x);

	//-----------------//
	//  DDTs(n, x):    //
	//-----------------//
	// d^2 T_i / dx^2 at x, for all i=0..n; res[0] and res[1] are always set to 0;
	// pre-cond: len(res) == n+1:
	//
	void DDTs(int n, double x, double* res, bool permissive = false);

	//-----------------//
	//  SumDDT(a,x):   //
	//-----------------//
	//  Efficient evaluation of Sum'_{i=2}^{n} a_i d^2 T_i/dx, len(a) == n+1,
	//  a[0] and a[1] are actually ignored, so in this case Sum' = Sum:
	//
	double SumDDT(int n, double const* a, double x, bool permissive = false);

	//-----------------//
	//  Zeros(n)   :   //
	//-----------------//
	//  Fills in the "res" vector with "n" real zeros of T_n in [-1.0; +1.0].
	//  Pre-condition: len(res) == n:
	//
	void Zeros(int n, double* res);

	//-----------------//
	//  Alphas(n)  :   //
	//-----------------//
	//  Fills in "res" with (n+1) odd zeros of the Translated 2nd-kind Chebyshev
	//  polynomial Us_{2n}(x) of degree (2n) -- required for Everhart-Sorokin
	//  integrator. Pre-condition: len(res) == n+1:
	//
	void Alphas(int n,  double* res);

	//-----------------//
	//  Extrema(n) :   //
	//-----------------//
	//  Fills in the "res" vector with (n+1) extremum points of T_n in
	//  [-1.0; +1.0], n >= 1.   Raises exception for n <= 0. The end points
	//  (-1, +1) are included in the result: although T'(x) does not vanish
	//  there, T(x) still takes  its min/max  value  (+-1) there -- same as
	//  in internal extremal points. Pre-condition:  len(res) == n+1:
	//
	void Extrema(int n, double* res);

	//-----------------//
	//  Inflects(n):   //
	//-----------------//
	//  Fills in the "res" vector   with (n-2) inflection points of T_n  in
	//  [-1.0; +1.0], i.e. zeros of T''(x). Pre-condition: len(res) == n-2:
	//
	void Inflects(int n, double* res);
};

#endif  // PALLAS_ASTRODYN_CHEBYSHEV_H

