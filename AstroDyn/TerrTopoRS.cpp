// vim:ts=2
//===========================================================================//
//                            "TerrTopoRS.cpp":                              //
//              Terrestrial and TopoCentric Reference Systems                //
//                     (c) Dr Leonid Timochouk, 2011                         //
//===========================================================================//
#include "AstroDyn/TerrTopoRS.h"
#include "AstroDyn/Spherical.h"
#include <cfloat>
#include <stdexcept>

using namespace std;

namespace
{
	using namespace AstroDyn;

	//-------------------------------------------------------------------------//
	// WGS84 Ellipsoid Params:                                                 //
	//-------------------------------------------------------------------------//
	LenKM  const WGS84_a(6378.137);
	double const WGS84_f  = 1.0 / 298.25722356;
	double const WGS84_e2 = WGS84_f * (2.0 - WGS84_f);
}

namespace AstroDyn
{
	//=========================================================================//
	// WGS84:                                                                  //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// WGS84 Tuple Ctor:                                                       //
	//-------------------------------------------------------------------------//
	WGS84<SVType::P>::WGS84
	(
		TCG tcg,
		tuple<char,AngDeg,AngMin,AngSec> const& lambda,
		tuple<char,AngDeg,AngMin,AngSec> const& phi,
		LenM h
	)
	:	m_tcg(tcg),
		m_h(h)
	{
		char lambda_sign = '\0';
		switch (get<0>(lambda))
		{
		case 'E':
			lambda_sign = '+';
			break;
		case 'W':
			lambda_sign = '-';
			break;
		default:
			throw invalid_argument("WGS84 Tuple Ctor: lambda must be 'E' or 'W'");
		}
		m_lambda = Spherical::ToDeg
			(lambda_sign, get<1>(lambda), get<2>(lambda), get<3>(lambda));

		char phi_sign = '\0';
		switch (get<0>(phi))
		{
		case 'N':
			phi_sign = '+';
			break;
		case 'S':
			phi_sign = '-';
			break;
		default:
			throw invalid_argument("WGS84 Tuple Ctor: phi must be 'N' or 'S'");
		}
		m_phi    = Spherical::ToDeg
			(phi_sign,		get<1>(phi),		get<2>(phi),		get<3>(phi));
	}
																
	//-------------------------------------------------------------------------//
	// WGS84 ->  ITRS:                                                         //
	//-------------------------------------------------------------------------//
	ITRS<SVType::P> ToITRS(WGS84<SVType::P> const& wgs84)
	{
		ITRS<SVType::P> itrs;
		itrs.m_tcg = wgs84.m_tcg;

		// Convert angles to dim-less radians:
		double lambda = DimLess(DimLessRad(To_Angle_rad(wgs84.m_lambda)));
		double phi    = DimLess(DimLessRad(To_Angle_rad(wgs84.m_phi)));

		double cosL   = cos(lambda);
		double sinL   = sin(lambda);
		double cosP   = cos(phi);
		double sinP   = sin(phi);

		LenKM  v      = WGS84_a / sqrt(1.0 - WGS84_e2 * sinP * sinP);
		LenKM  h      = To_Length_km(wgs84.m_h);
		LenKM	 r_xy   = (v + h) * cosP;

		itrs.m_r.m_x  = r_xy * cosL;
		itrs.m_r.m_y  = r_xy * sinL;
		itrs.m_r.m_z  = ((1.0 - WGS84_e2) * v + h) * sinP;

		return itrs;
	}

	//-------------------------------------------------------------------------//
	// WGS84 <-  ITRS:                                                         //
	//-------------------------------------------------------------------------//
	WGS84<SVType::P> ToWGS84(ITRS<SVType::P> const& itrs)
	{
		WGS84<SVType::P>	wgs84;	// The result

		LenKM x    = itrs.m_r.m_x;
		LenKM y    = itrs.m_r.m_y;
		LenKM z    = itrs.m_r.m_z;
		LenKM r_xy = sqrt(x * x + y * y);

		// Set "m_phi" and "m_h" on the result:
		//
		if (IsZero(r_xy))
		{
			// Polar position:
			wgs84.m_h   = To_Length_m(z);

			if (IsNegative(z))
				wgs84.m_phi = AngDeg(-90.0);
			else
				wgs84.m_phi = AngDeg( 90.0);
		}
		else
		{
			// Generic case: Newton's solution method for "phi".
			double alpha = DimLess(z / r_xy);
			double beta  = DimLess(WGS84_a / r_xy) * WGS84_e2;

			// Initial approximation (because WGS84_e2 is small):
			double phi0  = atan(alpha);
			double cosP0 = 0.0;
			double v0    = 0.0;
			double err0  = HUGE_VAL;

			for (int i = 0; i < 1000; ++i)
			{
				cosP0        = cos(phi0);
				double sinP0 = sin(phi0);
				v0           = sqrt(1.0 - WGS84_e2 * sinP0 * sinP0);
				double secP0 = 1.0 / cosP0;

				double F     = sinP0 * (secP0 - beta / v0) - alpha;
				double F1    = secP0 *  secP0 - beta / (v0 * v0 * v0) * cosP0;

				double phi1  = phi0 - F / F1;
				double err1  = fabs(phi1 - phi0);
				if (err1 > err0)
					break;		 // Errors start to increase

				// Otherwise: next iteration:
				phi0 = phi1;
				err0 = err1;
			}
			// Have we achieved the required accuracy?
			if (err0 > 1000.0 * DBL_EPSILON)
				throw runtime_error("ITRS->WGS84: Divergence");

			// Otherwise: OK, got "phi" and then "h":
			wgs84.m_phi = To_Angle_deg(AngRad(phi0));
			wgs84.m_h   = To_Length_m(r_xy / cosP0 - WGS84_a / v0);
		}

		// Finally, set "m_lambda":
		//
		double lambda = atan(DimLess(y / abs(x)));
		if (IsNegative(x))
		{
			if (IsNegative(y))
				lambda -= M_PI;
			else
				lambda += M_PI;	// Incl. y==0, i.e. for 180 deg, use E
		}
		wgs84.m_lambda = To_Angle_deg(AngRad(lambda));

		// Return the result:
		return wgs84;
	}
}
