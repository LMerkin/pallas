#include "AstroDyn/GeoCRS.h"
#include "AstroDyn/TimeScales.h"
#include "AstroDyn/Spherical.h"
#include <iostream>
#include <cstdio>

using namespace std;
using namespace AstroDyn;

int main(int argc, char const* argv[])
{
	if (argc < 4 || argc > 7)
	{
		cerr << "PARAMS: Year Month Day [hour [min [sec]]]" << endl;
		return 1;
	}

	// Get the UTC:
	int    Year  = atoi(argv[1]);
	int    Month = atoi(argv[2]);
	int    Day   = atoi(argv[3]);
	int    hour  = (argc >= 5) ? atoi(argv[4]) : 0;
	int    min   = (argc >= 6) ? atoi(argv[5]) : 0;
	double sec   = (argc >= 7) ? atof(argv[6]) : 0.0;

	printf("UTC   =   %04d-%02d-%02d %02d:%02d:%06.3f\n",
				  Year, Month, Day, hour, min, sec);
	UTC utc(Year, Month, Day, hour, min, sec);

	// Convert it into TDB (via TAI):
	TDB tdb(ToTDB(ToTT(TAI(utc))));

	// Set up HCRS at that time with the curr position at the origin
	// (Sun centre), incl velocity:
	HCRS<SVType::PV>  hcrs
	{
		tdb,
		{LenAU(),  LenAU(),  LenAU() },
		{VelAUD(), VelAUD(), VelAUD()}
	};

	// Convert this position into GCRS1:
	GCRS1<SVType::PV> gcrs1 = ToGCRS1(hcrs, ImplType::SOFA);

	// Compute the angular pos and vel in the ICRS, i.e. already equatorial
	// (J2000.0):
	auto apv =
		Spherical::R2S<LengthUnits::AU, TimeUnits::day>(gcrs1.m_r, gcrs1.m_v);

	auto pos = apv.first;
	auto vel = apv.second;

	// RA and Decl in fractional degress:
	AngDeg alpha0 = get<0>(pos);
	AngDeg delta0 = get<1>(pos);
	LenAU  rho0   = get<2>(pos);
	double cosd   = ::cos(DimLess(DimLessRad(To_Angle_rad(delta0))));

	// And velocities:
	AngVelDegD alpha0_dot = get<0>(vel);
	AngVelDegD delta0_dot = get<1>(vel);
	VelAUD		 rho0_dot		= get<2>(vel);

	// RA in HMS, Decl in DMS:
	auto alpha1 = Spherical::ToHMS(alpha0);
	auto delta1 = Spherical::ToDMS(delta0);

	// Print-out:
	printf("alpha =   %02.0fh %02.0fm %06.3fs\n",
					Magnitude(get<0>(alpha1)), Magnitude(get<1>(alpha1)),
					Magnitude(get<2>(alpha1)));

	printf("delta = %c %02.0fd %02.0f' %06.3f\"\n",
					get<0>(delta1),						 Magnitude(get<1>(delta1)),
					Magnitude(get<2>(delta1)), Magnitude(get<3>(delta1)));

	printf("rho   = %.15f %s\n\n", Magnitude(rho0), DimStr(rho0).c_str());

	// NB: we output alpha_dot * cos(delta):
	printf("a_dot = %9.5f \"/h\n",
		Magnitude(To_Angle_sec(To_Time_hour(alpha0_dot * cosd))));

	printf("d_dot = %9.5f \"/h\n",
		Magnitude(To_Time_hour(To_Angle_sec(delta0_dot))));

	printf("r_dot = %.15f km/s\n",
		Magnitude(To_Length_km(To_Time_sec(rho0_dot))));

	fflush(stdout);
	return 0;
}
