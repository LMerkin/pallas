#include "AstroDyn/TerrTopoRS.h"
#include <cstdio>
using namespace AstroDyn;
using namespace std;

int main()
{
	// NB: Time is irrelevant below:
	//
	WGS84<SVType::P> bw0
	{
		TCG(),
		make_tuple('E', AngDeg( 9.0), AngMin(10.0), AngSec(30.6113)),
		make_tuple('N', AngDeg(48.0), AngMin(46.0), AngSec(59.6564)),
		LenM(330.397)
	};

	printf("BW0     : lambda=%.15f deg, phi=%.15f deg, h = %.3f m\n\n",
					Magnitude(bw0.m_lambda), Magnitude(bw0.m_phi), Magnitude(bw0.m_h));

	ITRS<SVType::P> itrs0
	{
		TCG(),
		{LenKM(4156.93996), LenKM( 671.42874), LenKM(4774.95821)}
	};

	// WGS84 -> ITRS:
	ITRS<SVType::P> itrs1 = ToITRS(bw0);

	printf("ITRS1   : x = %.5f km, y = %.5f km, z = %.5f km\n",
								 Magnitude(itrs1.m_r.m_x), Magnitude(itrs1.m_r.m_y),
								 Magnitude(itrs1.m_r.m_z));
	printf("Expected: x = %.5f km, y = %.5f km, z = %.5f km\n",
								 Magnitude(itrs0.m_r.m_x), Magnitude(itrs0.m_r.m_y),
								 Magnitude(itrs0.m_r.m_z));

	// ITRS -> WGS84:
	WGS84<SVType::P> bw1 = ToWGS84(itrs0);

	printf("BW1     : lambda=%.15f deg, phi=%.15f deg, h = %.3f m\n\n",
					Magnitude(bw1.m_lambda), Magnitude(bw1.m_phi), Magnitude(bw1.m_h));

	// Transform itrs1 back: should get the result very close to bw0:
	WGS84<SVType::P> bw2 = ToWGS84(itrs1);

	printf("BW2     : lambda=%.15f deg, phi=%.15f deg, h = %.3f m\n",
					Magnitude(bw2.m_lambda), Magnitude(bw2.m_phi), Magnitude(bw2.m_h));
	printf("Expected: lambda=%.15f deg, phi=%.15f deg, h = %.3f m\n",
					Magnitude(bw0.m_lambda), Magnitude(bw0.m_phi), Magnitude(bw0.m_h));

	return 0;
}
