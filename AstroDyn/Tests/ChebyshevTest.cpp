// vim:ts=2
//===========================================================================//
//                           "ChebyshevTest.h":                              //
//               Test of Chebyshev Polynomials Implementation                //
//                       (c) Dr Leonid Timochouk, 2011                       //
//===========================================================================//
#include "AstroDyn/Chebyshev.h"
#include <iostream>
#include <cmath>
#include <cfloat>
#include <ctime>

using namespace Chebyshev;
using namespace std;

int main(int argc, char const* argv[])
{
	// Set the Max Degree if specified in the command line:
	int n = 0;
	if (argc >= 2)
		n = ::atoi(argv[1]);

	// Initialise the RNG:
	::srand48(time(NULL));

	// Testing vars:
	double max_err;
	double res;

	// Generate the maximum degree "n" randomly if it is not given explicitly:
	if (n <= 0)
		n =  int(::floor(255.0 * ::drand48())) + 2;

	cout << "Degree n = " << n << endl;

	// Generate a random point in [-1.0; +1.0]:
	double x =  2.0 * ::drand48() - 1.0;
	cout << "Point  x = " << x << endl;
	cout << "Mach.Eps = " << DBL_EPSILON << endl;

	// Vector of random weights in [0.0; +1.0]:
	double a[n+1];

	for (int j = 0; j <= n; ++j)
		a[j] = ::drand48();

	//---------//
	// TEST 1: //
	//---------//
	// T_n(zeros_j) == 0.0 indeed:
	//
	cout << "TEST 1: T_n(zeros)    == 0\t\t... ";

	double zeros[n];
	Zeros(n, zeros);

	max_err = 0.0;
	for (int j = 0; j < n; ++j)
	{
		res = ::fabs(T(n, zeros[j]));

		if (res > max_err)
			max_err = res;
	}
	cout << "MaxErr = " << max_err << endl;

	//---------//
	// TEST 2: //
	//---------//
	// T_0, ..., T_n : one go -vs- one-by-one:
	cout << "TEST 2: T_{0..n}(x)\t\t\t... ";

	double vals[n+1];
	Ts(n, x, vals);

	max_err = 0.0;
	for (int j = 0; j <= n; ++j)
	{
		res = ::fabs(T(j, x) - vals[j]);

		if (res > max_err)
			max_err = res;
	}
	cout << "MaxErr = " << max_err << endl;

	//---------//
	// TEST 3: //
	//---------//
	// T_n(extrema_j) == +-1.0 indeed:
	//
	cout << "TEST 3: T_n(extrema)  == +-1\t\t... ";

	double extrema[n+1];
	Extrema(n, extrema);

	max_err    =  0.0;
	double pm1 =  1.0;

	for (int j = n; j >= 0; --j)
	{
		res = ::fabs(T(n, extrema[j]) - pm1);

		if (res > max_err)
			max_err = res;
		pm1 = - pm1;
	}
	cout << "MaxErr = " << max_err << endl;

	//---------//
	// TEST 4: //
	//---------//
	// T'_0, ..., T'_n: one go -vs- one-by-one:
	//
	cout << "TEST 4: T'_{0..n}(x)\t\t\t... ";

	double ders[n+1];
	DTs(n, x, ders);

	max_err = 0.0;
	for (int j = 0; j <= n; ++j)
	{
		res = ::fabs(DT(j, x) - ders[j]);

		if (res > max_err)
			max_err = res;
	}
	cout << "MaxErr = " << max_err << endl;

	//---------//
	// TEST 5: //
	//---------//
	// T'_n(extrema_j) == 0 indeed (internal points only):
	//
	cout << "TEST 5: T'_n(extrema) == 0\t\t... ";
	max_err = 0.0;

	for (int j = 1; j < n; ++j)
	{
		res = ::fabs(DT(n, extrema[j]));

		if (res > max_err)
			max_err = res;
	}
	cout << "MaxErr = " << max_err << endl;

	//---------//
	// TEST 6: //
	//---------//
	// T'_{2n+1}(alphas_j) == 0 indeed (j >= 1):
	//
	cout << "TEST 6: T'_{2n+1}(alphas) == 0\t\t... ";

	double alphas[n+1];
	Alphas(n, alphas);

	max_err = 0.0;
	int m = 2*n + 1;

	for (int j = 1; j <= n; ++j)
	{
		double x = 2.0 * alphas[j] - 1.0;
		res = ::fabs(DT(m, x)) / double(m);

		if (res > max_err)
			max_err = res;
	}
	cout << "MaxErr = " << max_err << endl;

	//---------//
	// TEST 7: //
	//---------//
	// T"_0, ..., T"_n: one go -vs- one-by-one:
	//
	cout << "TEST 7: T\"_{0..n}(x)\t\t\t... ";

	double ddrs[n+1];
	DDTs(n, x, ddrs);

	max_err = 0.0;
	for (int j = 0; j <= n; ++j)
	{
		res = ::fabs(DDT(j, x) - ddrs[j]);

		if (res > max_err)
			max_err = res;
	}
	cout << "MaxErr = " << max_err << endl;

	//---------//
	// TEST 8: //
	//---------//
	// T"_n(inflects_j) == 0 indeed:
	//
	if (n >= 3)
	{
		cout << "TEST 8: T\"_n(inflects) == 0\t\t... ";

		double inflects[n-2];
		Inflects(n, inflects);

		max_err = 0.0;

		for (int j = 0; j < n-2; ++j)
		{
			res = ::fabs(DDT(n, inflects[j]));

			if (res > max_err)
			max_err = res;
		}
		cout << "MaxErr = " << max_err << endl;
	}
	else
		cout << "TEST 8: T\"_n(inflects) == 0\t\t... SKIPPED" << endl;

	//---------//
	// TEST 9: //
	//---------//
	cout << "TEST 9: T\"_n(+-1)\t\t\t... ";

	max_err     = ::fabs(DDT(n,  1.0) - DDTpm1(n,  1.0));
	double atm1 = ::fabs(DDT(n, -1.0) - DDTpm1(n, -1.0));
	if (atm1 > max_err)
		max_err = atm1;

	cout << "MaxErr = " << max_err << endl;

	//----------//
	// TEST 10: //
	//--------- //
	// Sum'_{i=0}^n a_i T_i(x) : 2 ways...
	//
	cout << "TEST 10: Sum'_{i=0}^n a_i T_i(x)\t... ";

	res = - Sum1T(n, a, x);
	for (int j = 0; j <= n; ++j)
	{
		double c  = (j == 0) ? (0.5 * a[0]) : a[j];
		res += c * T(j, x);
	}
	cout << "MaxErr = " << ::fabs(res) << endl;

	//----------//
	// TEST 11: //
	//----------//
	// Sum_{i=1}^n a_i T'_i(x): 2 ways...
	//
	cout << "TEST 11: Sum_{i=1}^n a_i T'_i(x)\t... ";

	res = - SumDT(n, a, x);
	for (int j = 0; j <= n; ++j) // Incl j==0 as well
		res += a[j] * DT(j, x);

	cout << "MaxErr = " << ::fabs(res) << endl;

	//----------//
	// TEST 12: //
	//----------//
	// Sum_{i=2}^n a_i T"_i(x): 2 ways...
	//
	cout << "TEST 12: Sum_{i=2}^n a_i T\"_i(x)\t... ";

	res = - SumDDT(n, a, x);
	for (int j = 0; j <= n; ++j) // Incl j=0, j=1 as well
		res += a[j] * DDT(j, x);

	cout << "MaxErr = " << ::fabs(res) << endl;
};

