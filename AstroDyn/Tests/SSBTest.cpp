#include "AstroDyn/TimeScales.h"
#include "AstroDyn/BaryHelioCRS.h"
#include <cstdio>

using namespace AstroDyn;
using namespace std;

int main()
{
	// SSB in HelioCentric co-ordinates:
	TDB t0 = ToTDB(ToTT(TAI(UTC(1945, 1, 1, 0, 0, 0.0))));
	TDB t1 = ToTDB(ToTT(TAI(UTC(1997, 1, 1, 0, 0, 0.0))));

	HCRS<SVType::P> HCOrigin;	// Co-ords are initialised to 0

	for (TDB t = t0; t <= t1; t += TimeD(1.0))
	{
		HCOrigin.m_tdb = t;
		BCRS<SVType::P> SSB = ToBCRS(HCOrigin, ImplType::SOFA);

		printf("%.10e\t%.10e\n",
					 Magnitude(SSB.m_r.m_x), Magnitude(SSB.m_r.m_y));
	}
	return 0;
}
