#include "AstroDyn/TimeScales.h"
#include <iostream>
#include <cstdio>
#include <cassert>
using namespace std;
using namespace AstroDyn;

int main(int argc, char const* argv[])
{
	if (argc < 4 || argc > 7)
	{
		cerr << "PARAMS: Year Month Day [hour [min [sec]]]" << endl;
		return 1;
	}
	int 	 Year  = atoi(argv[1]);
	int 	 Month = atoi(argv[2]);
	int 	 Day   = atoi(argv[3]);
	int 	 hour  = (argc >= 5) ? atoi(argv[4]) : 0;
	int 	 min   = (argc >= 6) ? atoi(argv[5]) : 0;
	double sec	 = (argc >= 7) ? atof(argv[6]) : 0.0;

	TimeD EPS(1e-16);

	printf("UTC0 = %04d-%02d-%02d %02d:%02d:%010.7f\n",
				 Year, Month, Day, hour, min, sec);

	printf("UTC->TAI:\n");
	UTC   utc0(Year, Month, Day, hour, min, sec);
	TimeS dat0 = DeltaAT(utc0);
	TAI   tai0 = TAI(utc0);

	printf("TAI0 = %.16f %s,  DAT = %f %s\n",
					Magnitude(MJD(tai0)), DimStr(MJD(tai0)).c_str(),
					Magnitude(dat0),      DimStr(dat0).c_str());

	printf("TAI->UTC:\n");
	UTC   utc1(tai0);
	printf("UTC1 = %04d-%02d-%02d %02d:%02d:%010.7f, DAT = %f %s\n",
				utc1.Year(), utc1.Month(), utc1.Day(),
				utc1.hour(), utc1.min(),	 utc1.sec(),
				Magnitude(utc1.DeltaAT()), DimStr(utc1.DeltaAT()).c_str());

	printf("TAI->TDB:\n");
	TDB   tdb0 = ToTDB(ToTT(tai0));
	printf("TDB0 = %.16f %s\n", Magnitude(MJD(tdb0)), DimStr(MJD(tdb0)).c_str());

	printf("TDB->TT :\n");
	TT    tt0  = ToTT(tdb0);
	printf("TT0  = %.16f %s\n", Magnitude(MJD(tt0)),  DimStr(MJD(tt0)).c_str());

	printf("TT ->TDB:\n");
	TDB   tdb1 = ToTDB(tt0);
	printf("TDB1 = %.16f %s\n", Magnitude(MJD(tdb1)), DimStr(MJD(tdb1)).c_str());
	fflush(stdout);
	assert(abs(tdb1 - tdb0) < EPS);

	printf("TDB->TAI:\n");
	TAI   tai1 = ToTAI(ToTT(tdb1));
	printf("TAI1 = %.16f %s\n", Magnitude(MJD(tai1)), DimStr(MJD(tai1)).c_str());
	fflush(stdout);
	assert(abs(tai1 - tai0) < EPS);

	printf("TDB->TCB:\n");
	TCB   tcb0 = ToTCB(tdb1);
	printf("TCB0 = %.16f %s\n", Magnitude(MJD(tcb0)), DimStr(MJD(tcb0)).c_str());

	printf("TCB->TDB:\n");
	TDB   tdb2 = ToTDB(tcb0);
	printf("TDB2 = %.16f %s\n", Magnitude(MJD(tdb2)), DimStr(MJD(tdb2)).c_str());
	fflush(stdout);
	assert(abs(tdb2 - tdb1) < EPS && abs(tdb2 - tdb0) < EPS);

	printf("TT ->TCG:\n");
	TCG   tcg0 = ToTCG(tt0);
	printf("TCG0 = %.16f %s\n", Magnitude(MJD(tcg0)), DimStr(MJD(tcg0)).c_str());

	printf("TCG->TT :\n");
	TT    tt1  = ToTT(tcg0);
	printf("TT1  = %.16f %s\n", Magnitude(MJD(tt1)),  DimStr(MJD(tt1)).c_str());
	fflush(stdout);
	assert(abs(tt1 - tt0) < EPS);

	return 0;
}
