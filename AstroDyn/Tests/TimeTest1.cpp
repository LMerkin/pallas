#include "AstroDyn/TimeScales.h"
#include <cstdio>
#include <cassert>
#include <cmath>
using namespace std;
using namespace AstroDyn;

int main()
{
	TT 	j2000 = J2000();
	TAI	tai		= ToTAI(j2000);
	UTC utc		= UTC(tai);

	// Print out TAI. Strictly speaking, TAI is a sequantial count of seconds, so
	// it has no notion of Year, ..., sec;   for this reason no standard calendar
	// functions are provided for TAI;   we extract the day fraction manually and
	// convert it into no-leap-seconds hh:mm:ss:
	//
	double mjd = Magnitude(tai.MJD());
	assert(mjd > 0.0);

	double df  = mjd - floor(mjd);
	assert(df  > 0.0);

	int    hh  = int(floor(df * 24.0));
	int    mm  = int(floor(df * 1440.0)) - hh * 60;
	double ss  = df * 86400.0 - double(3600 * hh + 60 * mm);

	printf("TAI(J2000.0)  = xxxx-xx-xx %02d:%02d:%010.7f\n", hh, mm, ss);
	printf("Expected Value: 2000-01-01 11:59:27.816\n\n");

	// Print out the corresp UTC:
	printf("UTC(J2000.0)  = %04d-%02d-%02d %02d:%02d:%010.7f\n",
		utc.Year(), utc.Month(), utc.Day(),
		utc.hour(), utc.min(),   utc.sec());
	printf("Expected Value: 2000-01-01 11:58:55.816\n");

	fflush(stdout);
	return 0;
}
