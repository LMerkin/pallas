// vim:ts=2
//===========================================================================//
//                                "Sun_SOFA.h":                              //
//                SSB-to-Sun Poisson Series According to SOFA                //
//                       (c) Dr Leonid Timochouk, 2011                       //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_SUN_SOFA_H
#define PALLAS_ASTRODYN_SUN_SOFA_H

#include "AstroDyn/PoissonSOFA.h"

namespace ImplSOFA
{
	extern RawPoissonSeries33 const SSB2Sun;
}

#endif	// PALLAS_ASTRODYN_SUN_SOFA_H

