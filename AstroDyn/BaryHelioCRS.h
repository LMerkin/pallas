// vim:ts=2
//===========================================================================//
//                             "BaryHelioCRS.h":                             //
//   Solar System BaryCentric and HelioCentric Celestial Reference Systems   //
//                      (c) Dr Leonid Timochouk, 2011                        //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_BARYHELIOCRS_H
#define PALLAS_ASTRODYN_BARYHELIOCRS_H

#include "AstroDyn/TimeScales.h"

namespace AstroDyn
{
	//=========================================================================//
	// "BCRS": Solar System BaryCentric Celestial Reference System:            //
	//=========================================================================//
	// The co-ordinate frame is Rectangular and ICRF-aligned.
	// This is an "absolutely inertial" system (not reducible to any other one).
	// The following structure represents Position, Velocity and Acceleration of
	// a point mass in this frame:
	//
	template<SVType S>
	struct BCRS;					// Explicitly specialised for all "SVType" values

	template<>
	struct BCRS<SVType::P>
	{
		TDB					m_tdb;	// Time
		V3<LenAU>		m_r;		// Position

		// Ctors:
		BCRS(TDB  tdb, V3<LenAU> const& r)
			:	m_tdb(tdb), m_r(r) {}
		BCRS() = default;
	};

	template<>
	struct BCRS<SVType::PV>
	{
		TDB					m_tdb;	// Time
		V3<LenAU>		m_r;		// Position
		V3<VelAUD>	m_v;		// Velocity

		// Ctors:
		BCRS(TDB  tdb, V3<LenAU> const& r, V3<VelAUD> const& v)
			:	m_tdb(tdb), m_r(r), m_v(v) {}
		BCRS() = default;
	};

	template<>
	struct BCRS<SVType::PVA>
	{
		TDB					m_tdb;	// Time
		V3<LenAU>		m_r;		// Position
		V3<VelAUD>	m_v;		// Velocity
		V3<AccAUD2>	m_a;		// Acceleration

		// Ctors:
		BCRS(TDB  tdb, V3<LenAU> const& r, V3<VelAUD> const& v,
				 V3<AccAUD2> const& a)
			:	m_tdb(tdb), m_r(r), m_v(v), m_a(a) {}
		BCRS() = default;
	};

	//=========================================================================//
	// "HCRS": HelioCentric Celestial Rectangular Reference System:            //
	//=========================================================================//
	// Same ICRF frame as for "BCRS" but with the HelioCentric origin:
	//
	template<SVType S>
	struct HCRS;					// Explicitly specialised for all "SVType" values

	template<>
	struct HCRS<SVType::P>
	{
		TDB					m_tdb;	// Time
		V3<LenAU>		m_r;		// Position

		// Ctors:
		HCRS(TDB  tdb, V3<LenAU> const& r)
			:	m_tdb(tdb), m_r(r) {}
		HCRS() = default;
	};

	// Transforms HCRS <-> BCRS:
	HCRS<SVType::P> ToHCRS
			(BCRS<SVType::P> const& bcrs, ImplType I = ImplType::SOFA);
	BCRS<SVType::P> ToBCRS
			(HCRS<SVType::P> const& hcrs, ImplType I = ImplType::SOFA);

	template<>
	struct HCRS<SVType::PV>
	{
		TDB					m_tdb;	// Time
		V3<LenAU>		m_r;		// Position
		V3<VelAUD>	m_v;		// Velocity

		// Ctors:
		HCRS(TDB  tdb, V3<LenAU> const& r, V3<VelAUD> const& v)
			:	m_tdb(tdb), m_r(r), m_v(v) {}
		HCRS() = default;
	};

	// Transforms HCRS <-> BCRS:
	HCRS<SVType::PV> ToHCRS
			(BCRS<SVType::PV> const& bcrs, ImplType I = ImplType::SOFA);
	BCRS<SVType::PV> ToBCRS
			(HCRS<SVType::PV> const& hcrs, ImplType I = ImplType::SOFA);

	template<>
	struct HCRS<SVType::PVA>
	{
		TDB					m_tdb;	// Time
		V3<LenAU>		m_r;		// Position
		V3<VelAUD>	m_v;		// Velocity
		V3<AccAUD2>	m_a;		// Acceleration

		// Ctors:
		HCRS(TDB  tdb, V3<LenAU> const& r, V3<VelAUD> const& v,
				 V3<AccAUD2> const& a)
			:	m_tdb(tdb), m_r(r), m_v(v), m_a(a) {}
		HCRS() = default;
	};

	// Transforms HCRS <-> BCRS:
	HCRS<SVType::PVA> ToHCRS
			(BCRS<SVType::PVA> const& bcrs, ImplType I = ImplType::SOFA);
	BCRS<SVType::PVA> ToVCRS
			(HCRS<SVType::PVA> const& hcrs, ImplType I = ImplType::SOFA);
}

#endif // PALLAS_ASTRODYN_BARYHELIOCRS_H
