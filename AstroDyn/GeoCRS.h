// vim:ts=2
//===========================================================================//
//                                "GeoCRS.h":                                //
//                      GeoCentric Reference  Systems                        //
//                      (c) Dr Leonid Timochouk, 2011                        //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_GEOCRS_H
#define PALLAS_ASTRODYN_GEOCRS_H

#include "AstroDyn/TimeScales.h"
#include "AstroDyn/BaryHelioCRS.h"

namespace AstroDyn
{
	//=========================================================================//
	// "GCRS*": GeoCentric Celestial Rectangular Reference Systems:            //
	//=========================================================================//
	// For GeoCentric Frames, Time is TCG, not TDB!
	// Same ICRF inertial (non-rotating) rectangular frame as for BCRS and HCRS,
	// but with the GeoCentric origin.
	// Moreover, 2 GCRS systems are provided which differ by the units used.
	//-------------------------------------------------------------------------//
	// GCRS1: AU, day                                                          //
	//-------------------------------------------------------------------------//
	//
	template<SVType S>
	struct GCRS1;				// Explicitly specialised for all "SVType" values

	template<>
	struct GCRS1<SVType::P>
	{
		TCG       	m_tcg;  // Time
		V3<LenAU>		m_r;		// Position

		// Ctors:
		GCRS1(TCG tcg, V3<LenAU> const& r)
			:	m_tcg(tcg), m_r(r) {}
		GCRS1() = default;
	};

	// Transforms GCRS1 <-> HCRS:
	GCRS1<SVType::P> ToGCRS1
			(HCRS<SVType::P>  const& hcrs,  ImplType I = ImplType::SOFA);
	HCRS<SVType::P>  ToHCRS
			(GCRS1<SVType::P> const& gcrs1, ImplType I = ImplType::SOFA);

	template<>
	struct GCRS1<SVType::PV>
	{
		TCG					m_tcg;	// Time
		V3<LenAU>		m_r;		// Position
		V3<VelAUD>	m_v;		// Velocity

		// Ctors:
		GCRS1(TCG tcg, V3<LenAU> const& r, V3<VelAUD> const& v)
			:	m_tcg(tcg), m_r(r), m_v(v) {}
		GCRS1() = default;
	};

	// Transforms GCRS1 <-> HCRS:
	GCRS1<SVType::PV> ToGCRS1
			(HCRS<SVType::PV>  const& hcrs,  ImplType I = ImplType::SOFA);
	HCRS<SVType::PV>  ToHCRS
			(GCRS1<SVType::PV> const& gcrs1, ImplType I = ImplType::SOFA);

	template<>
	struct GCRS1<SVType::PVA>
	{
		TCG					m_tcg;	// Time
		V3<LenAU>		m_r;		// Position
		V3<VelAUD>	m_v;		// Velocity
		V3<AccAUD2>	m_a;		// Acceleration

		// Ctors:
		GCRS1(TCG tcg, V3<LenAU> const& r, V3<VelAUD> const& v,
					V3<AccAUD2> const& a)
			: m_tcg(tcg), m_r(r), m_v(v), m_a(a) {}
		GCRS1() = default;
	};

	// Transforms GCRS1 <-> HCRS:
	GCRS1<SVType::PVA> ToGCRS1
			(HCRS<SVType::PVA>  const& hcrs,  ImplType I = ImplType::SOFA);
	HCRS<SVType::PVA>  ToHCRS
			(GCRS1<SVType::PVA> const& gcrs1, ImplType I = ImplType::SOFA);

	//-------------------------------------------------------------------------//
	// GCRS2: km, sec                                                          //
	//-------------------------------------------------------------------------//
	template<SVType S>
	struct GCRS2;					// Explicitly specialised for all "SVType" values

	template<>
	struct GCRS2<SVType::P>
	{
		TCG					m_tcg;	// Time
		V3<LenKM>		m_r;		// Position

		// Ctors:
		GCRS2(TCG tcg, V3<LenKM> const& r)
			: m_tcg(tcg), m_r(r) {}
		GCRS2() = default;
	};

	// Transforms GCRS2 <-> GCRS1; NB: no "ImplType" param here!
	GCRS2<SVType::P> ToGCRS2(GCRS1<SVType::P> const& gcrs1);
	GCRS1<SVType::P> ToGCRS1(GCRS2<SVType::P> const& gcrs2);

	template<>
	struct GCRS2<SVType::PV>
	{
		TCG					m_tcg;	// Time
		V3<LenKM>		m_r;		// Position
		V3<VelKMS>	m_v;		// Velocity

		// Ctors:
		GCRS2(TCG tcg, V3<LenKM> const& r, V3<VelKMS> const& v)
			:	m_tcg(tcg), m_r(r), m_v(v) {}
		GCRS2() = default;
	};

	// Transforms GCRS2 <-> GCRS1; NB: no "ImplType" param here!
	GCRS2<SVType::PV> ToGCRS2(GCRS1<SVType::PV> const& gcrs1);
	GCRS1<SVType::PV> ToGCRS1(GCRS2<SVType::PV> const& gcrs2);

	template<>
	struct GCRS2<SVType::PVA>
	{
		TCG					m_tcg;	// Time
		V3<LenKM>		m_r;		// Position
		V3<VelKMS>	m_v;		// Velocity
		V3<AccKMS2>	m_a;		// Acceleration

		// Ctors:
		GCRS2(TCG tcg, V3<LenKM> const& r, V3<VelKMS> const& v,
					V3<AccKMS2> const& a)
			:	m_tcg(tcg), m_r(r), m_v(v), m_a(a) {}
		GCRS2() = default;
	};

	// Transforms GCRS2 <-> GCRS1; NB: no "ImplType" param here!
	GCRS2<SVType::PVA> ToGCRS2(GCRS1<SVType::PVA> const& gcrs1);
	GCRS1<SVType::PVA> ToGCRS1(GCRS2<SVType::PVA> const& gcrs2);
}

#endif // PALLAS_ASTRODYN_GEOCRS_H
