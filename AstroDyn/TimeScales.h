// vim:ts=2
//===========================================================================//
//                            "TimeScales.h":                                //
//             Time Scales etc for Various Reference Frames                  //
//                     (c) Dr Leonid Timochouk, 2011                         //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_TIMESCALES_H
#define PALLAS_ASTRODYN_TIMESCALES_H

#include "AstroDyn/CommonTypes.h"

namespace AstroDyn
{
	//=========================================================================//
	// Uniform Time Scales (NOT UTC!!!)                                        //
	//=========================================================================//
	// Time line is MJD, the unit is day.
	// (*) TAI: Atomic Time
	// (*) TT:  TT == TAI + 32.184 sec exactly; same as TDT and ET;
	// (*) TDB: Dynamic BaryCentric Time.
	// 		 Approximately TDB ~= TT == TAI + 32.184 sec, but there are minor
	// 		 periodic variations of < 2 msec due to relativistic effects. TDB
	// 		 is the independent arg in the ephemerides computations:
	//
	enum class TSType
	{
		TAI			 = 0,
		TAI_GPS  = 1,
		TT			 = 2,	// Same as TDT and ET
		TDB			 = 3,
		TCB			 = 4,
		TCG			 = 5
	};

	template<TSType S>
	class TimeScale
	{
	private:
		TimeD	m_MJD;

	public:
		// Ctors (copy ctor, assignment and equality are auto-generated):
		TimeScale() = default;
		explicit TimeScale(TimeD const& mjd): m_MJD(mjd)  {};

		// Accessors:
		TimeD const& MJD() const
			{ return m_MJD; }

		friend TimeD const& MJD(TimeScale const& right)
			{ return right.MJD(); }

		// Interval arithmetic. NB: "TimeScale" is for the time line, "TimeD" is for
		// intervals:
		TimeScale operator+(TimeD const& right) const
			{ return TimeScale(m_MJD + right); }

		TimeScale& operator+=(TimeD const& right)
			{ m_MJD += right; return *this;   }

		friend TimeScale operator+(TimeD const& left, TimeScale const& right)
			{ return TimeScale(left + right.m_MJD); }

		TimeScale operator-(TimeD const& right) const
			{ return TimeScale(m_MJD - right); }

		TimeScale& operator-=(TimeD const& right)
			{ m_MJD -= right; return *this;   }

		TimeD operator-(TimeScale const& right) const
			{ return m_MJD - right.m_MJD;     }

		// For convenience, adding / subtracting "TimeS" intervals is also supp'd:
		TimeScale operator+(TimeS const& right) const
			{ return TimeScale(m_MJD + To_Time_day(right));	}

		TimeScale& operator+=(TimeS const& right)
		{
			m_MJD += To_Time_day(right);
			return *this;
		}

		friend TimeScale operator+(TimeS const& left, TimeScale const& right)
			{ return TimeScale(To_Time_day(left) + right.m_MJD);	}

		TimeScale operator-(TimeS const& right) const
			{ return TimeScale(m_MJD - To_Time_day(right));				}

		TimeScale& operator-=(TimeS const& right)
		{
			m_MJD -= To_Time_day(right);
			return *this;
		}

		// Comparisons lifted from "TimeD":
		bool operator< (TimeScale const& right) const
			{ return m_MJD <  right.m_MJD;	}

		bool operator<=(TimeScale const& right)	const
			{ return m_MJD <= right.m_MJD;	}

		bool operator> (TimeScale const& right)	const
			{ return m_MJD >  right.m_MJD;	}

		bool operator>=(TimeScale const& right)	const
			{ return m_MJD >= right.m_MJD;	}
	};

	//=========================================================================//
	// Specific Time Scales: TAI, TAI_GPS, TT, TDB, TCB, TCG:                  //
	//=========================================================================//
	typedef TimeScale<TSType::TAI>			TAI;
	typedef TimeScale<TSType::TAI_GPS>	TAI_GPS;
	typedef TimeScale<TSType::TT>				TT;		// Same as TDT and ET
	typedef TimeScale<TSType::TDB>			TDB;
	typedef TimeScale<TSType::TCB>			TCB;
	typedef TimeScale<TSType::TCG>			TCG;

	//-------------------------------------------------------------------------//
	// Conversion functions between the above scales:                          //
	//-------------------------------------------------------------------------//
	// TAI <---> TT <~~~> TDB
	//  ^        ^         ^
	//  |        |         |
	//  v        v         v
	// TAI_GPS   TCG      TCB

	// TAI <-> TAI_GPS:
	//
	inline TAI ToTAI(TAI_GPS tai_gps)
		{ return TAI(tai_gps.MJD() + To_Time_day(TimeS(19.0))); }

	inline TAI_GPS ToTAI_GPS(TAI tai)
		{ return TAI_GPS(tai.MJD() - To_Time_day(TimeS(19.0))); }

	// TAI <-> TT:
	//
	inline TAI ToTAI(TT tt)
		{ return TAI(tt.MJD() - To_Time_day(TimeS(32.184))); }

	inline TT  ToTT (TAI tai)
		{ return TT(tai.MJD() + To_Time_day(TimeS(32.184))); }

	// TT <~> TDB:
	// Transform contains periodic terms -- complex implementation, not inlined:
	//
	TDB ToTDB(TT   tt);
	TT  ToTT (TDB tdb);

	// TDB <-> TCB:
	// Contain re-scaling:
	//
	TCB ToTCB(TDB tdb);
	TDB ToTDB(TCB tcb);

	// TT <-> TCG:
	// Contain re-scaling:
	//
	TCG ToTCG(TT tt  );
	TT  ToTT (TCG tcg);

	// The Epoch J2000.0: is actually TT = 2000-01-01 12:00:00 (i.e. NOON, not
	// mid-night), hence the 0.5 fractional part:
	//
	inline TT J2000()						{ return TT(TimeD(51544.5)); }

	//=========================================================================//
	// UTC: "Civilian Time-Keeping System":                                    //
	//=========================================================================//
	// UT time is non-uniform because it is based on Earth rotation: the day dura-
	// tion is non-constant due to introduction of leap seconds.  For this reason,
	// there is no point in JD representation;  Gregorian calendar representation
	// is used; MJD is stored for the corresp TAI value. There is no interval ari-
	// thmetic either.
	// Applicability range: 1960 (introduced) -- 2014 (curr. limit of leap second
	// prediction):
	class UTC
	{
	private:
		int			m_Year;			// From 1960 (UTC introduced), currently up to 2014
		int			m_Month;		// 1..12
		int			m_Day;			// 1..31 depending on "m_month"
		int			m_hour;			// 0..23
		int			m_min;			// 0..59
		double	m_sec;			// 0..60.xxx because of leap seconds
		TimeS		m_DeltaAT;	// Delta(AT), seconds
		TAI			m_TAI;			// No harm in storing this...

	public:
		// Default ctor: result is meaningless:
		UTC() = default;

		// Non-default ctor: calendar. In the strict mode, years outside of the
		// range specified above are rehected:
		UTC(int Year, int Month, int Day, int hour, int min, double sec,
				bool strict = false);

		// Non-default ctor: from TAI:
		explicit UTC(TAI const& tai);

		// Copy ctor, assignment, (==) and (!=) are auto-generated
		// Accessors:
		int 	 Year()		const 									{ return m_Year; 	}
		friend int Year(UTC const& right) 			{ return right.Year(); }

		int    Month()  const										{ return m_Month;	}
		friend int Month(UTC const& right)			{ return right.Month();}

		int    Day()		const 									{ return m_Day;	}
		friend int Day(UTC const& right)				{ return right.Day();  }

		int    hour()		const 									{ return m_hour;}
		friend int hour(UTC const& right) 			{ return right.hour(); }

		int    min()		const 									{ return m_min;	}
		friend int min(UTC const& right)				{ return right.min();  }

		double sec()		const 									{ return m_sec;	}
		friend double sec(UTC const& right)			{ return right.sec();  }

		TimeS	 DeltaAT()const										{ return m_DeltaAT; }
		friend TimeS DeltaAT(UTC const& right)	{ return right.DeltaAT(); }

		// Convertion to TAI:
		explicit operator TAI() const						{ return m_TAI; }
	};
}

#endif	// PALLAS_ASTRODYN_TIMESCALES_H
