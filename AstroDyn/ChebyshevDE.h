// vim:ts=2
//===========================================================================//
//                             "ChebyshevDE.h":                              //
//            Chebyshev Coeffivcients Structure as used in DE4XX             //
//                       (c) De Leonid Timochouk, 2011                       //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_CHEBYSHEVDE_H
#define PALLAS_ASTRODYN_CHEBYSHEVDE_H

#include "AstroDyn/TimeScales.h"

namespace ImplDE
{
	using namespace AstroDyn;

	//-------------------------------------------------------------------------//
	// Common Starting Date (all time instants are TDB):                       //
	//-------------------------------------------------------------------------//
	TDB const t0(TimeD(14992.0));

	//-------------------------------------------------------------------------//
	// DE4XX Chebyshev Coeffs Structure:                                       //
	//-------------------------------------------------------------------------//
	// S: Total Number of Steps (each of C co-ords, each of N coeffs)
	// C: Number of Co-Ords (typically 3, for nutations 2)
	// N: Number of Coeffs  per Co-Ord per Time
	//
	template<unsigned int S, unsigned int C, unsigned int N>
	struct ChebyshevDE
	{
		double const m_step;					// Days
		double const m_data[S][C][N];	// ICRF, KM
	};

	//-------------------------------------------------------------------------//
	// Data Objects:                                                           //
	//-------------------------------------------------------------------------//
	// Earth-Moon BaryCenter, SSB-Centric:
	extern ChebyshevDE<4568,  3, 13> const EMB_DE423;

	// Moon, GeoCentric:
	extern ChebyshevDE<18272, 3, 13> const Moon_DE423;
}

#endif // PALLAS_ASTRODYN_CHEBYSHEVDE_H
