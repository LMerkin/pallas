// vim:ts=2
//===========================================================================//
//                            "CommonTypes.h":                               //
//     Dimension Types Declarations for AstroDynamical Reference Frames      //
//                     (c) Dr Leonid Timochouk, 2011                         //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_COMMONTYPES_H
#define PALLAS_ASTRODYN_COMMONTYPES_H

#include "DimTypes/Dimensions.h"
#include <cmath>

//---------------------------------------------------------------------------//
// AstroDynamical Dimensions and Units:                                      //
//---------------------------------------------------------------------------//
// Must be declared in the global namespace:
//
DECLARE_DIMS(Length, Time, Angle)

DECLARE_DIM_UNITS(Length, km, m, AU)
DECLARE_UNIT(Length, m,  1e-3);
DECLARE_UNIT(Length, AU, 1.495978706996262e+08)		// From DE423

DECLARE_DIM_UNITS(Time, sec, min, hour, day, year)
DECLARE_UNIT(Time, min,     60.0)
DECLARE_UNIT(Time, hour,  3600.0)
DECLARE_UNIT(Time, day,  86400.0)
DECLARE_UNIT(Time, year, 86400.0 * 365.25);

DECLARE_DIM_UNITS(Angle, rad, deg, min, sec, hour, hmin, hsec)
DECLARE_UNIT(Angle, deg,  M_PI /    180.0);
DECLARE_UNIT(Angle, min,  M_PI /  10800.0);
DECLARE_UNIT(Angle, sec,  M_PI / 648000.0);
DECLARE_UNIT(Angle, hour, M_PI /     12.0);				// Typically for RA only
DECLARE_UNIT(Angle, hmin, M_PI /    720.0);				//
DECLARE_UNIT(Angle, hsec, M_PI /  43200.0); 			//

namespace AstroDyn
{
	//-------------------------------------------------------------------------//
	// Kinematic Types:                                                        //
	//-------------------------------------------------------------------------//
	enum class SVType	// State Vector Type
	{
		P   = 0,				// Only Position
		PV  = 1,				// Position and Velocity
		PVA = 2					// Position, Velocity and Acceleration
	};

	typedef decltype(Length_AU())             					LenAU;
	typedef decltype(Time_day())               					TimeD;
	typedef decltype(Time_year())												TimeY;
	typedef decltype(Length_AU() / Time_day())      		VelAUD;
	typedef decltype(Length_AU() / Time_day().pow<2>())	AccAUD2;

	typedef decltype(Length_km())												LenKM;
	typedef decltype(Length_m())												LenM;
	typedef decltype(Time_sec())												TimeS;
	typedef decltype(Length_km() / Time_sec())					VelKMS;
	typedef decltype(Length_km() / Time_sec().pow<2>())	AccKMS2;

	typedef decltype(Angle_rad())												AngRad;
	typedef decltype(Angle_deg())												AngDeg;
	typedef decltype(Angle_min())												AngMin;
	typedef decltype(Angle_sec())												AngSec;
	typedef decltype(Angle_hour())											AngHour;
	typedef decltype(Angle_hmin())											AngHMin;
	typedef	decltype(Angle_hsec())											AngHSec;
	typedef decltype(Angle_deg() / Time_sec())					AngVelDegS;
	typedef decltype(Angle_deg() / Time_day())					AngVelDegD;

	// 3D Vector types:
	//
	template<typename T>
	struct V3
	{
		T		m_x;
		T		m_y;
		T		m_z;
	};

	//-------------------------------------------------------------------------//
	// Implementation Selector:                                                //
	//-------------------------------------------------------------------------//
	// For some co-ordinate system transforms, there are several possible
	// implementations:
	//
	enum class ImplType
	{
		SOFA 		= 0,
		NOVAS 	= 1,
		DE4XX 	= 2,
		VSOP87	= 3
	};
}

//---------------------------------------------------------------------------//
// Angle Handling:                                                           //
//---------------------------------------------------------------------------//
// Angle expressed in radians can be converted to dim-less:
//
namespace ADDimH
{
	// Internal machinery:
	//
	constexpr unsigned long ZeroOutRad(unsigned long E, unsigned long U)
	{
		return
			DimH::GetFld(U, (unsigned int)(Dims::Angle)) ==
											(unsigned int)(AngleUnits::rad)
		? DimH::ZeroOutFld(E, (unsigned int)(Dims::Angle))
		: E;
	}
}

namespace AstroDyn
{
	// Externally-visible conversion:
	//
	template<unsigned long E, unsigned long U, typename T = double>
	inline DimQ<ADDimH::ZeroOutRad(E,U), ADDimH::ZeroOutRad(U,U), T>
	DimLessRad(DimQ<E,U,T> const& dimq)
	{
		return
		DimQ<ADDimH::ZeroOutRad(E,U), ADDimH::ZeroOutRad(U,U), T>(Magnitude(dimq));
	}
}

#endif	// PALLAS_ASTRODYN_COMMONTYPES_H
