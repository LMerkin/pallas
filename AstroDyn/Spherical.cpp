// vim:ts=2
//===========================================================================//
//                             "Spherical.cpp":                              //
//                      Generic Spherical Co-Ord Systems                     //
//                       (c) Dr Leonid Timochouk, 2011                       //
//===========================================================================//
#include "AstroDyn/Spherical.h"
#include <stdexcept>

namespace Spherical
{
	//-------------------------------------------------------------------------//
	// "ToHMS":                                                                //
	//-------------------------------------------------------------------------//
	// Deg (-180..+180) into HMS (0..24 hour with hmin & hsec):                //
	//
	tuple<AngHour,AngHMin,AngHSec> ToHMS(AngDeg deg)
	{
		if (!(Angle_deg(-180.0) < deg && deg <= Angle_deg(180.0)))
			throw invalid_argument("ToHMS: deg must be in (-180.0 .. +180.0]");

		if (IsNegative(deg))
			deg += Angle_deg(360.0);

		assert(Angle_deg(0.0) <= deg && deg < Angle_deg(360.0));

		// NB: "round" in "hmin" is only required to correct rounding errors;
		// without such errors, its arg should already be integral:
		AngHour hour = floor(To_Angle_hour(deg));
		AngHMin hmin = round(floor(To_Angle_hmin(deg)) - To_Angle_hmin(hour));
		AngHSec hsec = To_Angle_hsec(deg) - To_Angle_hsec(hmin) -
									 To_Angle_hsec(hour);

		assert(Angle_hour(0.0) <= hour && hour <= Angle_hour(23.0) &&
					 Angle_hmin(0.0) <= hmin && hmin <= Angle_hmin(59.0) &&
					 Angle_hsec(0.0) <= hsec && hsec <  Angle_hsec(60.0));

		return make_tuple(hour, hmin, hsec);
	}

	//-------------------------------------------------------------------------//
	// "ToDMS":                                                                //
	//-------------------------------------------------------------------------//
	// Deg (-180..+180) into DMS (same range but with min & sec):              //
	// The resulting components are always non-negative; if the arg was negative,
	// "sign" will be '-', otherwise '+':
	//
	tuple<char,AngDeg,AngMin,AngSec> ToDMS(AngDeg deg)
	{
		if (!(Angle_deg(-180.0) < deg && deg <= Angle_deg(180.0)))
			throw invalid_argument("ToDMS: deg must be in (-180.0 .. +180.0]");

		char osign = IsNegative(deg) ? '-' : '+';
		deg = abs(deg);

		// As in "ToHMS", "round" in "min" is only required to correct rounding
		// errors:
		AngDeg odeg = floor(deg);
		AngMin omin = round(floor(To_Angle_min(deg)) - To_Angle_min(odeg));
		AngSec osec = To_Angle_sec(deg) - To_Angle_sec(omin) - To_Angle_sec(odeg);

		assert(Angle_deg(0.0) <= odeg && odeg <= Angle_deg(179.0) &&
					 Angle_min(0.0) <= omin && omin <= Angle_min( 59.0) &&
					 Angle_sec(0.0) <= osec && osec <  Angle_sec( 60.0));

		return make_tuple(osign, odeg, omin, osec);
	}

	//-------------------------------------------------------------------------//
	// "ToDeg":                                                                //
	//-------------------------------------------------------------------------//
	// HMS (0..24 hour with hmin & hsec) to fractional deg; the args must be non-
	// negative; "hour" and "min" must be integral:
	//
	AngDeg ToDeg(AngHour hour, AngHMin hmin, AngHSec hsec)
	{
		if (!(AngHour(0.0) <= hour && hour <= AngHour(23.0) && ceil(hour) == hour &&
					AngHMin(0.0) <= hmin && hmin <= AngHMin(59.0) && ceil(hmin) == hmin &&
					AngHSec(0.0) <= hsec && hsec <  AngHSec(60.0)))
			throw invalid_argument("ToDeg: Invalid HMS arg(s)");

		return To_Angle_deg(hour) + To_Angle_deg(hmin) + To_Angle_deg(hsec);
	}

	// Deg with min and sec to fractional deg. The args must be non-negative,
	// "deg" and "min" must be intergral; "sign" is the sign of over-all expr
	// ('+' or '-'), the result is signed accordingly:
	//
	AngDeg ToDeg(char sign, AngDeg  deg,  AngMin  min,  AngSec  sec)
	{
		if (!((sign == '+' || sign == '-')																	 &&
					AngDeg(0.0) <= deg && deg <= AngDeg(179.0) && ceil(deg) == deg &&
					AngMin(0.0) <= min && min <= AngMin( 59.0) && ceil(min) == min &&
					AngSec(0.0) <= sec && sec <  AngSec( 60.0)))
			throw invalid_argument("ToDeg: Invalid DMS arg(s)");

		AngDeg res = deg + To_Angle_deg(min) + To_Angle_deg(sec);
		if (sign == '-')
			res = - res;
		return res;
	}
}
