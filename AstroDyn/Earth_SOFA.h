// vim:ts=2
//===========================================================================//
//                               "Earth_SOFA.h"                              //
//              Sun-to-Earth Poisson Series According to SOFA                //
//                       (c) Dr Leonid Timochouk, 2011                       //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_EARTH_SOFA_H
#define PALLAS_ASTRODYN_EARTH_SOFA_H

#include "AstroDyn/PoissonSOFA.h"

namespace ImplSOFA
{
	extern RawPoissonSeries33 const Sun2Earth;
}

#endif	// PALLAS_ASTRODYN_EARTH_SOFA_H

