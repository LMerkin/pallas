// vim:ts=2
//===========================================================================//
//                              "Spherical.h":                               //
//                      Generic Spherical Co-Ord Systems                     //
//                       (c) Dr Leonid Timochouk, 2011                       //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_SPHERICAL_H
#define PALLAS_ASTRODYN_SPHERICAL_H

#include "AstroDyn/CommonTypes.h"
#include <limits>
#include <cmath>
#include <cstdlib>
#include <tuple>
#include <utility>
#include <stdexcept>

#ifdef  NDEBUG
#define PALLAS_RESTORE_NDEBUG
#undef  NDEBUG
#endif 
#include <cassert>

//===========================================================================//
// Short-cuts for internal "DimQ" handling:                                  //
//===========================================================================//
namespace ADDimH
{
	//-------------------------------------------------------------------------//
	// Special Exponents:                                                      //
	//-------------------------------------------------------------------------//
	constexpr unsigned long LenE    = DimH::DimExp((unsigned int)(Dims::Length));
	constexpr unsigned long TimeE   = DimH::DimExp((unsigned int)(Dims::Time));
	constexpr unsigned long AngE    = DimH::DimExp((unsigned int)(Dims::Angle));
	constexpr unsigned long VelE    = DimH::SubExp(LenE, TimeE);
	constexpr unsigned long AngVelE = DimH::SubExp(AngE, TimeE);

	constexpr unsigned long MkLenU(LengthUnits LU)
		{ return DimH::MkUnit((unsigned int)(Dims::Length), (unsigned int)(LU)); }

	constexpr unsigned long MkVelU(LengthUnits LU, TimeUnits TU)
		{ return DimH::MkUnit((unsigned int)(Dims::Length), (unsigned int)(LU))  |
						 DimH::MkUnit((unsigned int)(Dims::Time),   (unsigned int)(TU));
		}

	constexpr unsigned long MkAngVelU(TimeUnits TU)	// Angle is always in deg
		{ return DimH::MkUnit((unsigned int)(Dims::Angle),
													(unsigned int)(AngleUnits::deg))                   |
						 DimH::MkUnit((unsigned int)(Dims::Time),   (unsigned int)(TU));
		}

	//-------------------------------------------------------------------------//
	// Compact declaration of types to be used in "Spherical":                 //
	//-------------------------------------------------------------------------//
	// Length type with Length Unit given by "LU":
	template<LengthUnits LU>
	struct Len
	{
		typedef DimQ<LenE, MkLenU(LU)> T;
	};

	// Velocity type with Length and Time units given by "LU" and "TU", resp:
	template<LengthUnits LU, TimeUnits TU>
	struct Vel
	{
		typedef DimQ<VelE, MkVelU(LU,TU)> T;
	};

	// Angular Velocity with Time Unit given by "TU", Angle unit being "deg":
	template<TimeUnits TU>
	struct AngVel
	{
		typedef DimQ<AngVelE, MkAngVelU(TU)> T;
	};
}

//===========================================================================//
// Externally-Visible "Speherical" Functions:                                //
//===========================================================================//
namespace Spherical
{
	using namespace AstroDyn;
	using namespace ADDimH;
	using namespace std;

	// NB:
	// (1) Transformations are currently provided only for Positions and Veloci-
	//		 ties, not for Accelerations  (the latter are rarely expressed in any
	//		 Spherical system).
	// (2) Length Unit: "LU" (template parameter);
	//		 Angle  Unit: always "deg";
	//		 Time   Unit: "TU" (template parameter).
	// (3) Transformation of Velocities may be skipped (if "WithVel" is not set).

	//=========================================================================//
	// Spherical -> Rectangular:                                               //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// "S2R": Position only:                                                   //
	//-------------------------------------------------------------------------//
	template<LengthUnits LU>
	inline V3<typename Len<LU>::T> S2R
	(
		AngDeg lambda,
		AngDeg phi,
		typename Len<LU>::T rho
	)
	{
		double lambdaR = DimLess(AstroDyn::DimLessRad(To_Angle_rad(lambda)));
		double phiR    = DimLess(AstroDyn::DimLessRad(To_Angle_rad(phi)));

		typename Len<LU>::T rhoXY = rho * cos(phiR);
		V3<typename Len<LU>::T> r
		{
			rhoXY * ::cos(lambdaR),
			rhoXY * ::sin(lambdaR),
			rho   * ::sin(phiR)
		};
		return r;
	}

	//-------------------------------------------------------------------------//
	// "S2R": Position and Velocity:                                           //
	//-------------------------------------------------------------------------//
	template<LengthUnits LU, TimeUnits TU>
	inline pair<V3<typename Len<LU>::T>, V3<typename Vel<LU,TU>::T> > S2R
	(
		AngDeg lambda,
		AngDeg phi,
		typename Len<LU>::T rho,

		typename AngVel<TU>::T lambda_dot,
		typename AngVel<TU>::T phi_dot,
		typename Vel<LU,TU>::T rho_dot
	)
	{
		double lambdaR = DimLess(AstroDyn::DimLessRad(To_Angle_rad(lambda)));
		double phiR    = DimLess(AstroDyn::DimLessRad(To_Angle_rad(phi)));

		double cp		= ::cos(phiR);
		double sp		= ::sin(phiR);
		double cl		= ::cos(lambdaR);
		double sl		= ::sin(lambdaR);

		double cpcl = cp * cl;
		double cpsl = cp * sl;

		V3<typename Len<LU>::T> r
		{
			rho * cpcl,
			rho * cpsl,
			rho * sp
		};
		typename Len<LU>::T rpd = rho * phi_dot;
		typename Len<LU>::T rld = rho * lambda_dot;

		V3<typename Vel<LU,TU>::T> r_dot
		{
			rho_dot * cpcl - rpd * sp * cl - rld * cpsl,
			rho_dot * cpsl - rpd * sp * sl + rld * cpcl,
			rho_dot * sp   + rpd * cp
		};

		return make_pair(r, r_dot);
	}

	//=========================================================================//
	// Rectangular -> Spherical:                                               //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// "R2S": Position only:                                                   //
	//-------------------------------------------------------------------------//
	template<LengthUnits LU>
	inline tuple<AngDeg, AngDeg, typename Len<LU>::T> R2S
		(V3<typename Len<LU>::T> const& r)
	{
		typename Len<LU>::T rho =
			sqrt(r.m_x * r.m_x + r.m_y * r.m_y + r.m_z * r.m_z);
		AngDeg  lambda = AngDeg(0.0);
		AngDeg  phi    = AngDeg(0.0);

		if (!IsZero(rho))
		{
			// Generic case (otherwise "lambda" and "phi" remain 0):
			phi    = To_Angle_deg(Angle_rad(::asin(DimLess(r.m_z / rho))));

			if (!(IsZero(r.m_x) && IsZero(r.m_y)))
				// (Otherwise pole, "lambda" is undefined, keep it at 0):
			{
				lambda = To_Angle_deg(Angle_rad(::atan(DimLess(r.m_y / r.m_x))));
				// This is correct for quadrants I and IV (x > 0), otherwise +- 180 deg:
				if (IsNegative(r.m_x))
				{
					if (IsNegative(r.m_y))
						lambda -= Angle_deg(180.0);
					else
						lambda += Angle_deg(180.0);	// Incl y==0, i.e. for 180 deg, use '+'
				}
			}
		}
		return tuple<AngDeg, AngDeg, typename Len<LU>::T>(lambda, phi, rho);
	}

	//-------------------------------------------------------------------------//
	// "R2S": Position and Velocity:                                           //
	//-------------------------------------------------------------------------//
	template<LengthUnits LU, TimeUnits TU>
	inline pair<tuple<AngDeg, AngDeg, typename Len<LU>::T>,
							tuple<typename AngVel<TU>::T, typename AngVel<TU>::T,
										typename Vel<LU,TU>::T> >
	R2S
	(
		V3<typename Len<LU>::T>    const& r,
		V3<typename Vel<LU,TU>::T> const& r_dot
	)
	{
		// Position (lambda, phi, rho):
		tuple<AngDeg, AngDeg, typename Len<LU>::T> pos = R2S<LU>(r);
		AngDeg lambda						= get<0>(pos);
		AngDeg phi							= get<1>(pos);
		typename Len<LU>::T rho = get<2>(pos);

		typename AngVel<TU>::T	lambda_dot;	// Initially 0
		typename AngVel<TU>::T	phi_dot;		// Initially 0
		typename Vel<LU,TU>::T	rho_dot;		// Initially 0

		typename Vel<LU,TU>::T	vx = r_dot.m_x;
		typename Vel<LU,TU>::T	vy = r_dot.m_y;
		typename Vel<LU,TU>::T	vz = r_dot.m_z;

		// Velocity:
		if (IsZero(rho))
		{
			// In the center:
			// "lambda" and "phi" are undefined and set to 0, so "lambda_dot" and
			// "phi_dot" are undefined as well, but "rho_dot" is defined:
			if (IsZero(vy) && IsZero(vz))
				rho_dot = vx;
			else
				throw runtime_error("R2S: Singularity at the Origin");
		}
		else
		{
			// Not in the center:
			assert(IsPositive(rho));

			double phiR		 = DimLess(DimLessRad(To_Angle_rad(phi)));
			double lambdaR = DimLess(DimLessRad(To_Angle_rad(lambda)));

			double cp			 = ::cos(phiR);
			double sp			 = ::sin(phiR);
			double cl			 = ::cos(lambdaR);
			double sl			 = ::sin(lambdaR);

			// NB: In the exprs below, to convert the results into  AngVel (deg/TU),
			// they need to be first notionally multiplied by 1 rad, otherwise they
			// would not contain angle at all:
			//
			AngRad Rad1(1.0);

			if (cp == 0.0)
				// At a pole: "lambda_dot" is undefined:
				if (IsZero(vy))
					if (sp > 0.0)
					{
						// Actually sp == 1.0:
						phi_dot	= To_Angle_deg(- vy / rho / sl * Angle_rad());
						rho_dot	= vz;
					}
					else
					{
						// sp == -1.0:
						phi_dot	 = To_Angle_deg( vy / rho / sl * Angle_rad());
						rho_dot	 = - vz;
					}
				else
					throw runtime_error("R2S: Singularity at a Pole");
			else
			{
				// Fully generic case: Invert the matrix for r_dot:
				lambda_dot =
					To_Angle_deg(      (-sl * vx + cl * vy) / rho / cp * Rad1);
				phi_dot    =
					To_Angle_deg((sp * (-cl * vx - sl * vy) + cp * vz) / rho * Rad1);

				rho_dot    =		cp * ( cl * vx + sl * vy) + sp * vz;
			}
		}
		tuple<typename AngVel<TU>::T, typename AngVel<TU>::T,
				  typename Vel<LU,TU>::T>
			vel(lambda_dot, phi_dot, rho_dot);

		return make_pair(pos, vel);
	}

	//========================================================================//
	// Angle Conversion Utils:                                                //
	//========================================================================//
	// Deg (-180..+180) into HMS (0..24 hour with hmin & hsec):
	tuple<AngHour,AngHMin,AngHSec> ToHMS(AngDeg deg);

	// Deg (-180..+180) into DMS (same range but with min & sec). The resulting
	// components are always non-negative; if the arg was negative, "sign" will
	// be '-', otherwise '+':
	tuple<char,AngDeg,AngMin,AngSec> ToDMS(AngDeg deg);

	// HMS (0..24 hour with hmin & hsec) to fractional deg; the args must be non-
	// negative; "hour" and "min" must be integral:
	AngDeg ToDeg(AngHour hour, AngHMin hmin, AngHSec hsec);

	// Deg with min and sec to fractional deg. The args must be non-negative,
	// "deg" and "min" must be intergral; "sign" is the sign of over-all expr
	// ('+' or '-'), the result is signed accordingly:
	AngDeg ToDeg(char sign, AngDeg  deg,  AngMin  min,  AngSec  sec);
}

#ifdef  PALLAS_RESTORE_NDEBUG
#define NDEBUG
#undef  PALLAS_RESTORE_NDEBUG
#endif

#endif // PALLAS_ASTRODYN_SPHERICAL_H
