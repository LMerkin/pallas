// vim:ts=2
//===========================================================================//
//                           "BaryHeloCRS.cpp":                              //
//         Solar System BaryCentric and HelioCentric Reference Systems       //
//                      (c) Dr Leonid Timochouk, 2011                        //
//===========================================================================//
#include "AstroDyn/BaryHelioCRS.h"
#include "AstroDyn/PoissonSOFA.h"
#include "AstroDyn/Sun_SOFA.h"
#include <stdexcept>
#include <cstdlib>

//===========================================================================//
// "SunInBCRS" Switch:                                                       //
//===========================================================================//
// Position, Velocity and Acceleration of the Sun in the BCRS system;
// Time arg is TDB;
// the data structures are compiled separately:
//
namespace
{
	using namespace AstroDyn;
	using namespace std;

	template<bool WithVel, bool WithAcc>
	void SunInBCRS(ImplType I, TDB tdb, LenAU r[3], VelAUD v[3], AccAUD2 a[3])
	{
		switch (I)
		{
		case ImplType::SOFA:
			ImplSOFA::PoissonCosSum<WithVel,WithAcc>
								(ImplSOFA::SSB2Sun, tdb, r, v, a);
			break;

		default:
			throw runtime_error("SunInBCRS: Unsupported ImplType");
		}
	}
}

//===========================================================================//
// Implementation of Externally-Visible Functions:                           //
//===========================================================================//
namespace AstroDyn
{
	// NB: no template<> decls anymore here!

	//-------------------------------------------------------------------------//
	// BCRS <-> HCRS: Pos only:                                                //
	//-------------------------------------------------------------------------//
	// HCRS <-  BCRS:
	HCRS<SVType::P> ToHCRS(BCRS<SVType::P> const& bcrs, ImplType I)
	{
		// Get the BCRS coords of the Sun:
		LenAU SunPos[3];
		SunInBCRS<false, false>(I, bcrs.m_tdb, SunPos, NULL, NULL);

		// Compute the relative HelioCentric vector:
		return HCRS<SVType::P>
		{
			bcrs.m_tdb,
			{
				bcrs.m_r.m_x - SunPos[0],
				bcrs.m_r.m_y - SunPos[1],
				bcrs.m_r.m_z - SunPos[2]
			}
		};
	}

	// BCRS <-  HCRS:
	BCRS<SVType::P> ToBCRS(HCRS<SVType::P> const& hcrs, ImplType I)
	{
		// Get the BCRS coords of the Sun:
		LenAU SunPos[3];
		SunInBCRS<false, false>(I, hcrs.m_tdb, SunPos, NULL, NULL);

		// Compute the absolute BaryCentric vector:
		return BCRS<SVType::P>
		{
			hcrs.m_tdb,
			{
				hcrs.m_r.m_x + SunPos[0],
				hcrs.m_r.m_y + SunPos[1],
				hcrs.m_r.m_z + SunPos[2]
			}
		};
	}

	//-------------------------------------------------------------------------//
	// BCRS <-> HCRS: Pos and Vel:                                             //
	//-------------------------------------------------------------------------//
	// HCRS <-  BCRS:
	HCRS<SVType::PV> ToHCRS(BCRS<SVType::PV> const& bcrs, ImplType I)
	{
		// Get the BCRS coords of the Sun:
		LenAU  SunPos[3];
		VelAUD SunVel[3];
		SunInBCRS<true, false>(I, bcrs.m_tdb, SunPos, SunVel, NULL);

		// Compute the relative HelioCentric vectors:
		return HCRS<SVType::PV>
		{
			bcrs.m_tdb,
			{
				bcrs.m_r.m_x - SunPos[0],
				bcrs.m_r.m_y - SunPos[1],
				bcrs.m_r.m_z - SunPos[2]
			},
			{
				bcrs.m_v.m_x - SunVel[0],
				bcrs.m_v.m_y - SunVel[1],
				bcrs.m_v.m_z - SunVel[2]
			}
		};
	}

	// BCRS <-  HCRS:
	BCRS<SVType::PV> ToBCRS(HCRS<SVType::PV> const& hcrs, ImplType I)
	{
		// Get the BCRS coords of the Sun:
		LenAU  SunPos[3];
		VelAUD SunVel[3];
		SunInBCRS<true, false>(I, hcrs.m_tdb, SunPos, SunVel, NULL);

		// Compute the absolute BaryCentric vectors:
		return BCRS<SVType::PV>
		{
			hcrs.m_tdb,
			{
				hcrs.m_r.m_x + SunPos[0],
				hcrs.m_r.m_y + SunPos[1],
				hcrs.m_r.m_z + SunPos[2]
			},
			{
				hcrs.m_v.m_x + SunVel[0],
				hcrs.m_v.m_y + SunVel[1],
				hcrs.m_v.m_z + SunVel[2]
			}
		};
	}

	//-------------------------------------------------------------------------//
	// BCRS <-> HCRS, Pos, Vel and Acc:                                        //
	//-------------------------------------------------------------------------//
	// HCRS <-  BCRS:
	HCRS<SVType::PVA> ToHCRS(BCRS<SVType::PVA> const& bcrs, ImplType I)
	{
		// Get the BCRS coords of the Sun:
		LenAU   SunPos[3];
		VelAUD  SunVel[3];
		AccAUD2 SunAcc[3];
		SunInBCRS<true, true>(I, bcrs.m_tdb, SunPos, SunVel, SunAcc);

		// Compute the relative HelioCentric vectors:
		return HCRS<SVType::PVA>
		{
			bcrs.m_tdb,
			{
				bcrs.m_r.m_x - SunPos[0],
				bcrs.m_r.m_y - SunPos[1],
				bcrs.m_r.m_z - SunPos[2]
			},
			{
				bcrs.m_v.m_x - SunVel[0],
				bcrs.m_v.m_y - SunVel[1],
				bcrs.m_v.m_z - SunVel[2]
			},
			{
				bcrs.m_a.m_x - SunAcc[0],
				bcrs.m_a.m_y - SunAcc[1],
				bcrs.m_a.m_z - SunAcc[2]
			}
		};
	}

	// BCRS <-  HCRS:
	BCRS<SVType::PVA> ToBCRS(HCRS<SVType::PVA> const& hcrs, ImplType I)
	{
		// Get the BCRS coords of the Sun:
		LenAU   SunPos[3];
		VelAUD  SunVel[3];
		AccAUD2 SunAcc[3];
		SunInBCRS<true, true>(I, hcrs.m_tdb, SunPos, SunVel, SunAcc);

		// Compute the absolute BaryCentric vectors:
		return BCRS<SVType::PVA>
		{
			hcrs.m_tdb,
			{
				hcrs.m_r.m_x + SunPos[0],
				hcrs.m_r.m_y + SunPos[1],
				hcrs.m_r.m_z + SunPos[2]
			},
			{
				hcrs.m_v.m_x + SunVel[0],
				hcrs.m_v.m_y + SunVel[1],
				hcrs.m_v.m_z + SunVel[2]
			},
			{
				hcrs.m_a.m_x + SunAcc[0],
				hcrs.m_a.m_y + SunAcc[1],
				hcrs.m_a.m_z + SunAcc[2]
			}
		};
	}
}
