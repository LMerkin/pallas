// vim:ts=2
//===========================================================================//
//                              "TerrTopoRS.h":                              //
//               Terrestrial and TopoCentric Reference Systems               //
//                        (c) Dr Leonid Timochouk, 2011                      //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_TERRTOPORS_H
#define PALLAS_ASTRODYN_TERRTOPORS_H

#include "AstroDyn/GeoCRS.h"
#include <tuple>

namespace AstroDyn
{
	//=========================================================================//
	// "ITRS": International Terrestrial Reference System                      //
	//=========================================================================//
	// The co-ordinate frame is Rectangular  but is NOT fixed  in the  inertial
	// space: it is connected to "tide-free" points on the Earth crust   and is
	// moving along with it. It experiences:
	// (*) daily rotation of the Earth (non-uniform, decelerating);
	// (*) movement of the poles (e.g. movement of the Earth crust relative  to
	//     the rotation axis);
	// (*) nutation -- short-periodic motion of the Earth rotation axis  in the
	// 		 inertial space;
	// (*) precession -- long-periodic motion of the Earth rotation axis in the
	//     inertial space:
	// NB: In terrestrial reference systems, the time is again TCG (as in GCRS*):
	//
	template<SVType S>
	struct ITRS;					// Explicitly specialised for all "SVType" values

	template<>
	struct ITRS<SVType::P>
	{
		TCG					m_tcg;	// Time
		V3<LenKM>		m_r;		// Position

		// Ctors:
		ITRS(TCG  tcg,  V3<LenKM> const& r)
			: m_tcg(tcg), m_r(r) {}
		ITRS() = default;
	};

	// Transforms ITRS <-> GCRS2:
	// XXX: TODO: Not implemented yet:
	ITRS<SVType::P>  ToITRS
			(GCRS2<SVType::P> const& gcrs2, ImplType I = ImplType::SOFA);
	GCRS2<SVType::P> ToGCRS2
			(ITRS<SVType::P>  const& itrs,  ImplType I = ImplType::SOFA);

	template<>
	struct ITRS<SVType::PV>
	{
		TCG					m_tcg;	// Time
		V3<LenKM>		m_r;		// Position
		V3<VelKMS>	m_v;		// Velocity

		// Ctors:
		ITRS(TCG  tcg,  V3<LenKM> const& r, V3<VelKMS> const& v)
			: m_tcg(tcg), m_r(r), m_v(v) {}
		ITRS() = default;
	};

	// Transforms ITRS <-> GCRS2:
	// XXX: TODO: Not implemented yet:
	ITRS<SVType::PV>  ToITRS
			(GCRS2<SVType::PV> const& gcrs2, ImplType I = ImplType::SOFA);
	GCRS2<SVType::PV> ToGCRS2
			(ITRS<SVType::PV>  const& itrs,  ImplType I = ImplType::SOFA);

	template<>
	struct ITRS<SVType::PVA>
	{
		TCG					m_tcg;	// Time
		V3<LenKM>		m_r;		// Position
		V3<VelKMS>	m_v;		// Velocity
		V3<AccKMS2>	m_a;		// Acceleration

		// Ctors:
		ITRS(TCG  tcg, V3<LenKM> const& r, V3<VelKMS> const& v,
				 V3<AccKMS2> const& a)
			:	m_tcg(tcg), m_r(r), m_v(v), m_a(a) {}
		ITRS() = default;
	};

	// Transforms ITRS <-> GCRS2:
	// XXX: TODO: Not implemented yet:
	ITRS<SVType::PVA>  ToITRS
			(GCRS2<SVType::PVA> const& gcrs2, ImplType I = ImplType::SOFA);
	GCRS2<SVType::PVA> ToGCRS2
			(ITRS<SVType::PVA>  const& itrs,  ImplType I = ImplType::SOFA);

	//=========================================================================//
	// WGS84: Geographic Co-Ordinates:                                         //
	//=========================================================================//
	// The GPS-compatible system of geographic co-ordinates. XXX: currently we
	// provide only Positions, not Velocities or Accelerations: this system is
	// used to specify the origins of TopoCentric systems in ITRS, but motions
	// *in* WGS84 are not considered:
	//
	template<SVType S>
	struct WGS84
	{
		static_assert(S != SVType::PV && S != SVType::PVA,
									"Velocities and Accelerations are not implemented in WGS84");
	};

	template<>
	struct WGS84<SVType::P>
	{
		TCG			m_tcg;		// Time
		AngDeg	m_lambda;	// Longitude, normally -180 .. +180 deg (W .. E)
		AngDeg	m_phi;		// Latitude,  normally  -90 ..  +90 deg (S .. N)
		LenM		m_h;			// Elevation (along the ellipsoidal normal) in meters

		// Ctor with tuples for "lambda" and "phi"; "char" args are 'E'/'W' and
		// 'N'/'S', resp:
		WGS84(TCG tcg,
					std::tuple<char,AngDeg,AngMin,AngSec> const& lambda,
					std::tuple<char,AngDeg,AngMin,AngSec> const& phi,
					LenM h);

		// Other ctors:
		WGS84(TCG tcg,  AngDeg   lambda, AngDeg phi, LenM h)
			: m_tcg(tcg), m_lambda(lambda), m_phi(phi), m_h(h) {}
		WGS84() = default;
	};

	// Transforms WGS84 <-> ITRS: NB: no "ImplType" param here!
	WGS84<SVType::P> ToWGS84(ITRS<SVType::P>  const& itrs);
	ITRS <SVType::P> ToITRS (WGS84<SVType::P> const& wgs84);
}

#endif // PALLAS_ASTRODYN_TERRTOPORS_H
