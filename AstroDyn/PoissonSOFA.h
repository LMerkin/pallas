// vim:ts=2
//===========================================================================//
//                             "PoissonSOFA.h":                              //
//          Representation and Summation of Poisson Series in SOFA           //
//                      (c) Dr Leonid Timochouk, 2011                        //
//                                                                           //
//         Contains code derived from IAU SOFA software of 2010-12-01        //
//===========================================================================//
#ifndef PALLAS_ASTRODYN_POISSONSOFA_H
#define PALLAS_ASTRODYN_POISSONSOFA_H

#include "AstroDyn/TimeScales.h"
#include <cmath>
#include <cstdlib>
#include <stdexcept>

#ifdef  NDEBUG
#define PALLAS_RESTORE_NDEBUG
#undef  NDEBUG
#endif
#include <cassert>

namespace ImplSOFA
{
	using namespace AstroDyn;
	using namespace std;

	//=========================================================================//
	// SOFA Poisson Series with Arbitrary Frequencies and Phases:              //
	//=========================================================================//
	// COSINE series with arbitrary Frequencies, Phases and Aplitues. Used in
	// computation of Sun and Earth position vectors. Because of this, Length
	// is always in AU and Time is in years internally but in days externally:
	//
	// Component for a given T^k and {X|Y|Z}. Consists of triples: {Amplitude,
	// Phase, Freq}:
	//
	struct APF
	{
		double const			m_a;					// Amplitude
		double const			m_p;					// Phase
		double const			m_f;					// Frequency
	};

	struct APFs
	{
		int const		 			m_N;					// Number of APF Triples
		APF const* const	m_Data;				// APF Triples
	};

	// Main struct: "RawPoissionSeries33" (3D: X,Y,Z and 3 powers of T: 0,1,2):
	struct RawPoissonSeries33
	{
		APFs const				m_T0[3];			// T^0:	X,Y,Z
		APFs const				m_T1[3];			// T^1:	X,Y,Z
		APFs const				m_T2[3];			// T^2:	X,Y,Z
	};

	//-------------------------------------------------------------------------//
	// "PoissonSumK":                                                          //
	//-------------------------------------------------------------------------//
	// Sum up a given T^k component:
	//
	typedef decltype(Time_year().pow<-1>())								FreqY;
	typedef decltype(Length_AU() / Time_year())           VelAUY;
	typedef decltype(Length_AU() / Time_year().pow<2>())  AccAUY2;


	template<unsigned int k, typename AT, bool WithVel, bool WithAcc>
	void PoissonSumK
	(
		APFs const apfs[3],						// X, Y, Z
		TimeY			 T,
		LenAU			 pos[3],
		VelAUY		 vel[3],
		AccAUY2		 acc[3]
	)
	{
		for (int i = 0; i < 3; ++i)		// X,Y,Z
		{
			int const	   N = apfs[i].m_N;
			APF const* apf = apfs[i].m_Data;

			for (int n = 0; n < N; ++n)
			{
				AT     a  = AT   (apf->m_a);
				FreqY  f  = FreqY(apf->m_f);
				double p  = DimLess(f * T) + apf->m_p;
				++apf;

				// NB: if it is P only, sin(p) is not required:
				double cosp = ::cos(p);
				double sinp = (WithVel || WithAcc) ? ::sin(p) : 0.0;

				// Position: always computed:
				pos[i] += a * pow<k>(T) * cosp;

				// Velocity:
				if (WithVel)
				{
					auto vk = - f * pow<k>(T) * sinp;
					if (k >= 1)
						vk += double(k) * pow<k-1>(T);
					vel[i] += a * vk;
				}

				// Acceleration:
				if (WithAcc)
				{
					auto ak = - f * f * pow<k>(T) * cosp;
					if (k >= 1)
						ak -= double(2*k) * f * pow<k-1>(T) * sinp;
					if (k >= 2)
						ak += double(k*(k-1)) * pow<k-2>(T) * cosp;
					acc[i] += a * ak;
				}
			}
		}
	}

	//-------------------------------------------------------------------------//
	// J2000 Ecliptics to DE405 ICRF conversion:                               //
	//-------------------------------------------------------------------------//
	// (To be used for co-ords computed in Ecliptical Poisson Series):
	//
	// The corresponding Euler angles are:
	//                       d  '  "
	//   1st rotation    -  23 26 21.4091 about the x-axis  (obliquity)
	//   2nd rotation    +         0.0475 about the z-axis  (RA offset)
	//
	// These were obtained empirically, by comparisons with DE405 over1900-2100:
	//
	template<unsigned long E, unsigned long U>
	inline void EclipticsToICRF(DimQ<E,U> vec[3])
	{
		assert(vec != NULL);
		double const am12 =  0.000000211284,
								 am13 = -0.000000091603,
								 am21 = -0.000000230286,
								 am22 =  0.917482137087,
								 am23 = -0.397776982902,
								 am32 =  0.397776982902,
								 am33 =  0.917482137087;
		DimQ<E,U>			 x1 =        vec[0] + am12 * vec[1] + am13 * vec[2],
									 y1 = am21 * vec[0] + am22 * vec[1] + am23 * vec[2],
									 z1 =                 am32 * vec[1] + am33 * vec[2];
		vec[0] = x1;
		vec[1] = y1;
		vec[2] = z1;
	}

	//-------------------------------------------------------------------------//
	// "PoissonCosSum":                                                        //
	//-------------------------------------------------------------------------//
	// A low-level function for summation of Cosine Poisson Series. Boolean
	// template params are for compile-time optimisation:
	//
	template<bool WithVel, bool WithAcc>
	void PoissonCosSum
	(
		RawPoissonSeries33 rps,
		TDB tdb,
		LenAU   r[3],
		VelAUD  v[3],	
		AccAUD2 a[3]
	)
	{
		assert(r != NULL && !(WithVel && (v == NULL)) && !(WithAcc && (a == NULL)));

		// Time since the reference epoch in Years:
		TimeY T = To_Time_year(tdb - ToTDB(J2000()));

		// This algorithm is only valid for |t| <= 100:
		if (abs(T) > TimeY(100.0))
			throw invalid_argument
						("ImplSOFA::PoissonCosSum: TDB Year must be in 1900 .. 2099");

		// Internal "vel" and "acc" use Year as the time unit. Create the output
		// vectors even if they are not to be used. Initially contains 0s:
		LenAU		pos[3];
		VelAUY  vel[3];
		AccAUY2 acc[3];

		// Sum up each T^k component:
		PoissonSumK<0, LenAU,   WithVel, WithAcc>(rps.m_T0, T, pos, vel, acc);
		PoissonSumK<1, VelAUY,  WithVel, WithAcc>(rps.m_T1, T, pos, vel, acc);
		PoissonSumK<2, AccAUY2, WithVel, WithAcc>(rps.m_T2, T, pos, vel, acc);

		// Rotate from J2000 Ecliptics to DE405 ICRF and change the Time unit from
		// years to days in "v" and "a" (if applicable):
		EclipticsToICRF(pos);
		r[0] = pos[0];
		r[1] = pos[1];
		r[2] = pos[2];

		if (WithVel)
		{
			EclipticsToICRF(vel);
			v[0] = To_Time_day(vel[0]);
			v[1] = To_Time_day(vel[1]);
			v[2] = To_Time_day(vel[2]);
		}

		if (WithAcc)
		{
			EclipticsToICRF(acc);
			a[0] = To_Time_day(acc[0]);
			a[1] = To_Time_day(acc[1]);
			a[2] = To_Time_day(acc[2]);
		}
	}
}

#ifdef  PALLAS_RESTORE_NDEBUG
#define NDEBUG
#undef  PALLAS_RESTORE_NDEBUG
#endif
#endif	// PALLAS_ASTRODYN_POISSONSOFA_H
