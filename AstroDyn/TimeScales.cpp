// vim:ts=2
//===========================================================================//
//                            "TimeScales.cpp":                              //
//                       Time Scale Transformations                          //
//                      (c) Dr Leonid Timochouk, 2011                        //
//                                                                           //
//         Contains code derived from IAU SOFA software of 2010-12-01        //
//===========================================================================//
#include "AstroDyn/TimeScales.h"

#include <stdexcept>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cfloat>
#undef NDEBUG
#include <cassert>

namespace
{
	using namespace std;
	using namespace AstroDyn;

	//=========================================================================//
	// Utils:                                                                  //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// Modified Julian Dates:                                                  //
	//-------------------------------------------------------------------------//
	// This is a simple calendar day count which is done irrespective  to the un-
	// derlying time scale (which may actually be non-uniform in seconds):
	//
	long ToMJD(int Year, int Month, int Day)
	{
    return
			long(367 * Year
					- (7 * (Year + (Month + 9) / 12)) / 4
					- (3 * ((Year + (Month - 9) / 7) / 100 + 1)) / 4
					+ (275 * Month) / 9
					+ Day
					- 678972);
	}

	void FromMJD(long MJD, int* Year, int* Month, int* Day)
	{
		long J = MJD + 2468570L;
    long C = (4L * J) / 146097L;
    J -= (146097L * C + 3L) / 4L;
    long Y = (4000L * (J + 1L)) / 1461001L;
    J -= (1461L * Y) / 4L - 31L;
    long M = (80L * J) / 2447L;

    *Day   = int(J - (2447L * M) / 80L);
    J = M / 11L;
    *Month = int(M + 2L - 12L * J);
    *Year  = int(100L * (C - 49L) + Y + J);
	}

	//-------------------------------------------------------------------------//
	// Computing Delta(AT) = TAI - UTC:                                        //
	//-------------------------------------------------------------------------//
	// Nodes for a piece-wide-linear function for Delta(AT).
	// Steps occur on the 1.0 day of stepYear, stepMonth (the prev day ends by a
	// leap sec if there is no drift):
	//
	struct DATNode
	{
		TimeD	 m_StepUTC;		// MJD UTC, date only
		TimeS  m_StepVal;		// sec
		TAI		 m_StepTAI;		// Moment when step is in effect, e.g. AFTER leap sec
		TimeD	 m_DriftUTC;	// MJD UTC, date only
		double m_DriftRate;	// Drift, dim-less, initially given as sec / day

		DATNode(int stepYear,      int stepMonth,     double stepVal,
						long driftUTC = 0, double driftRate = 0.0)

		: m_StepUTC(TimeD(double(ToMJD(stepYear, stepMonth, 1)))),
			m_StepVal(TimeS(stepVal)),
			m_StepTAI(TAI(m_StepUTC) + m_StepVal),
			m_DriftUTC(TimeD(double(driftUTC))),
			m_DriftRate(DimLess(Time_sec(driftRate) / To_Time_sec(Time_day())))
		{
			// NB: Also need to correct "m_StepTAI" for drift (if present):
			if (m_DriftRate != 0.0)
				m_StepTAI += m_DriftRate * (m_StepUTC - m_DriftUTC);
		}
	};

	vector<DATNode>	const dats
	{																							// Drift UTC dates:
  	{1960,  1,  1.4178180, 37300, 0.0012960},		// 1961-1.0
    {1961,  1,  1.4228180, 37300, 0.0012960},		// 1961-1.0
    {1961,  8,  1.3728180, 37300, 0.0012960},		// 1961-1.0
    {1962,  1,  1.8458580, 37665, 0.0011232},		// 1962-1.0
    {1963, 11,  1.9458580, 37665, 0.0011232},		// 1962-1.0
    {1964,  1,  3.2401300, 38761, 0.0012960},		// 1965-1.0
    {1964,  4,  3.3401300, 38761, 0.0012960},		// 1965-1.0
    {1964,  9,  3.4401300, 38761, 0.0012960},		// 1965-1.0
    {1965,  1,  3.5401300, 38761, 0.0012960},		// 1965-1.0
    {1965,  3,  3.6401300, 38761, 0.0012960},		// 1965-1.0
    {1965,  7,  3.7401300, 38761, 0.0012960},		// 1965-1.0
    {1965,  9,  3.8401300, 38761, 0.0012960},		// 1965-1.0
    {1966,  1,  4.3131700, 39126, 0.0025920},		// 1966-1.0
    {1968,  2,  4.2131700, 39126, 0.0025920},		// 1966-1.0
    {1972,  1,  10.0},
    {1972,  7,  11.0},
    {1973,  1,  12.0},
    {1974,  1,  13.0},
    {1975,  1,  14.0},
    {1976,  1,  15.0},
    {1977,  1,  16.0},
    {1978,  1,  17.0},
    {1979,  1,  18.0},
    {1980,  1,  19.0},
    {1981,  7,  20.0},
    {1982,  7,  21.0},
    {1983,  7,  22.0},
    {1985,  7,  23.0},
    {1988,  1,  24.0},
    {1990,  1,  25.0},
    {1991,  1,  26.0},
    {1992,  7,  27.0},
    {1993,  7,  28.0},
    {1994,  7,  29.0},
    {1996,  1,  30.0},
    {1997,  7,  31.0},
    {1999,  1,  32.0},
    {2006,  1,  33.0},
    {2009,  1,  34.0}
   };

	//-------------------------------------------------------------------------//
	// Validation of Calendar Dates (Proleptic Gregorian) and Times:           //
	//-------------------------------------------------------------------------//
	inline int MonthLength(int Year, int Month)
	{
		return
			(Month == 4 || Month == 6 || Month == 9 || Month == 11)
		? 30
		: (Month == 2)
		? (Year % 4 != 0 || (Year % 100 == 0 && Year % 400 != 0)
			 ? 28
			 : 29)
		: 31;
	}

	inline void ValiDate(int Year, int Month, int Day)
	{
		if (Year < -4799 || Month < 1 || Month > 12 || Day < 1 ||
				Day  > MonthLength(Year, Month))
			throw invalid_argument("Invalid Gregorian Date");
	}

	void ValiTime(int Year, int Month, int Day, int hour, int min, double sec)
	{
		if (hour < 0 || hour > 23 || min < 0 || min > 59 || sec < 0.0 ||
				sec >= 61.0)
			throw invalid_argument("Invalid UTC Time");

		if (sec < 60.0)
			return;	// OK

		// Furthermore, the 60th sec is only rarely valid ("Leap Second"). It may
		// only occur at the end of the last day of a month.   XXX: Negative leap
		// seconds have never been used, and are not considered here:
		bool PossLeapSec =
			(hour == 23) && (min == 59) && (Day == MonthLength(Year, Month));
		if (PossLeapSec)
		{
			// Now check the date: on the *next* day there must be an integral jump
			// in Delta(AT), so scan the table:
			Day = 1;
			if (++Month > 12)
				{ Month = 1; ++Year; }

			// NB: disregard early nodes containing drift -- they had no leap seconds;
			// "find" is more appropriate here than "binary_search", the cost diff is
			// negligible. Older nodes containing drift are skipped:
			TimeD currMJD = TimeD(double(ToMJD(Year, Month, Day)));
			vector<DATNode>::const_iterator it =
				find_if
				(dats.begin(), dats.end(),
				 [&](DATNode const& right) -> bool
						{ return right.m_StepUTC == currMJD && right.m_DriftRate == 0.0; }
				);
			PossLeapSec = (it != dats.end());
		}
		if (!PossLeapSec)
			throw invalid_argument("Invalid Leap Second");
		// Otherwise, the check is OK
	}

	//-------------------------------------------------------------------------//
	// TDB-TT Expansion:                                                       //
	//-------------------------------------------------------------------------//
	// Taken from the model of L.Fairhead & P.Bretagnon (1990) but all terms with
	// the total remaining Euclidian norm < 0.5 microseconds are OMITTED, in par-
	// ticular ALL TOPOCENTRIC TERMS, as higher accuracy is not representable in
	// the MJD format anyway (rel error > 1e-16, i.e. abs error > 0.4 us):
	//
	typedef vector<vector<double>> vec2d;
	typedef vector<vec2d>					 vec3d;

	vec3d const fbs =
	{
		// fb0:
		{
			{ 1656.674564e-6,     6283.075849991,  6.240054195 },
			{   22.417471e-6,     5753.384884897,  4.296977442 },
			{   13.839792e-6,    12566.151699983,  6.196904410 },
			{    4.770086e-6,      529.690965095,  0.444401603 },
			{    4.676740e-6,     6069.776754553,  4.021195093 },
			{    2.256707e-6,      213.299095438,  5.543113262 },
			{    1.694205e-6,      -3.523118349,   5.025132748 },
			{    1.554905e-6,    77713.771467920,  5.198467090 },
			{    1.276839e-6,     7860.419392439,  5.988822341 },
			{    1.193379e-6,     5223.693919802,  3.649823730 },
			{    1.115322e-6,     3930.209696220,  1.422745069 },
			{    0.794185e-6,    11506.769769794,  2.322313077 },
			{    0.600309e-6,     1577.343542448,  2.678271909 },
			{    0.496817e-6,     6208.294251424,  5.696701824 },
			{    0.486306e-6,     5884.926846583,  0.520007179 },
			{    0.468597e-6,     6244.942814354,  5.866398759 },
			{    0.447061e-6,       26.298319800,  3.615796498 },
			{    0.435206e-6,     -398.149003408,  4.349338347 },
			{    0.432392e-6,       74.781598567,  2.435898309 },
			{    0.375510e-6,     5507.553238667,  4.103476804 },
			{    0.243085e-6,     -775.522611324,  3.651837925 },
 			{    0.230685e-6,     5856.477659115,  4.773852582 },
			{    0.203747e-6,    12036.460734888,  4.333987818 }
		},
		// fb1:
		{
			{  102.156724e-6,     6283.075849991,  4.249032005 },
			{    1.706807e-6,    12566.151699983,  4.205904248 }
		},
		// fb2:
		{
			{    4.322990e-6,     6283.075849991,  2.642893748 }
		}
	};

	// NB: in the following function, it does not really matter in which time
	// scale (e.g., TAI, TT, TDB) the "mjd" arg is represented  -- the effect
	// of different scales is negligible:
	//
	TimeS TDBmTT(TimeD const& mjd)
	{
		// Time in Julian millenia since J2000.0. NB: it does not matter if J2000
		// is actually a TT or another time scale  -- the resulting error in T is
		// negligible:
		//
		double T = DimLess((mjd - J2000().MJD()) / To_Time_day(TimeY(1000.0)));

		// Iterate over "fbs" vectors. The outer dimension (corresp to powers of T)
		// is iterated backwards in order to compute the polynomial expr for "w":
		double w = 0.0;

		for (vec3d::const_reverse_iterator fbit = fbs.rbegin();
				 fbit != fbs.rend();  ++fbit)
		{
			double wi = 0.0;
			for (vec2d::const_iterator it = fbit->begin(); it != fbit->end(); ++it)
			{
				wi += (*it)[0] * sin((*it)[1] * T + (*it)[2]);

				if (fbit == fbs.rend())
					w = wi;			// Highest power ot T
				else
				{
					w *= T;			// All other T terms
					w += wi;
				}
			}
		}
		return TimeS(w);	// The result is in seconds
	}

	//-------------------------------------------------------------------------//
	// Re-Scaling Consts:                                                      //
	//-------------------------------------------------------------------------//
	double const LB = 1.550519768e-8;
	double const LG = 6.969290134e-10;
	TAI    const TAI1977(TimeD(43144.0));
}

namespace AstroDyn
{
	//=========================================================================//
	// UTC Functions:                                                          //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// UTC Calendar Ctor:                                                      //
	//-------------------------------------------------------------------------//
	UTC::UTC(int Year, int Month, int Day, int hour, int min, double sec,
					 bool strict)
	: m_Year(Year),
		m_Month(Month),
		m_Day(Day),
		m_hour(hour),
		m_min(min),
		m_sec(sec)
	{
		// Validate the fields:
		ValiDate(Year, Month, Day);
		ValiTime(Year, Month, Day, hour, min, sec);

		// Enforce the valid UTC range in the strict mode:
		if (strict && (Year < 1960 || Year > 2014))
			throw invalid_argument("UTC strict mode: Year must be in 1960..2014");

		// Compute MJD, Day Fraction, Delta(AT) and TAI fiels. Initial values are
		// unadjusted for Delta(AT):
		TimeD MJDcurr = TimeD(double(ToMJD(Year, Month, Day)));
		TimeD tai 		= MJDcurr;
		tai					 += To_Time_day(Time_hour(double(hour)));
		tai					 += To_Time_day(Time_min( double(min)) );
		tai					 += To_Time_day(Time_sec( double(sec)) );

		m_TAI			= TAI(tai);
		m_DeltaAT	= TimeS(0.0);

		if (Year < 1960)
			return;		// Nothing else to do

		// Otherwise:
		// Find the latest node <= MJDcurr = (Year, Month, Day):
		vector<DATNode>::const_reverse_iterator rit =
			find_if(dats.rbegin(), dats.rend(),
			 				[=](DATNode const& right) -> bool
				  			 { return right.m_StepUTC <= MJDcurr; }
						 );
		// Apply the step found:
		assert(rit != dats.rend());
		m_DeltaAT = rit->m_StepVal;

		// Check if the drift is also to be applied.   NB: The origin of the drift
		// term is considered to be in UTC, not TAI -- the difference is negligible
		// anyway:
		if (rit->m_DriftRate != 0.0)
			m_DeltaAT +=
				To_Time_sec(rit->m_DriftRate * (m_TAI.MJD() - rit->m_DriftUTC));

		// Now update m_TAI as well:
		m_TAI += m_DeltaAT;
	}

	//-------------------------------------------------------------------------//
	// UTC TAI Ctor:                                                           //
	//-------------------------------------------------------------------------//
	UTC::UTC(TAI const& tai)
	: m_TAI(tai)
	{
		// Find the effective Delta(AT); to that end, find the latest node with
		// m_StepTAI <= tai	(NB: use m_StepTAI, not m_StepUTC!!!):
		vector<DATNode>::const_reverse_iterator rit =
			find_if(dats.rbegin(), dats.rend(),
							[&](DATNode const& right) -> bool
								 { return right.m_StepTAI <= tai; }
						 );
		// Delta(AT) from the step, if found:
		m_DeltaAT = (rit != dats.rend()) ? rit->m_StepVal : TimeS(0.0);

		// May also need to apply the drift which is itself based on the UTC value.
		// Solve the following eqn:
		// dat =  Step + DriftRate * (utc - DriftUTC)
		// 		 =  Step + DriftRate * (tai - dat - DriftUTC)
		// =>
		// dat = (step + DriftRate * (tai - DriftUTC)) / (1 + DriftRate)
		//
		if (rit != dats.rend() && rit->m_DriftRate != 0.0)
			m_DeltaAT =
				(rit->m_StepVal +
				 To_Time_sec(rit->m_DriftRate * (m_TAI.MJD() - rit->m_DriftUTC))) /
				(1.0 + rit->m_DriftRate);

		// UTC MJD: XXX: not using "DimQ" for "utc" and "dFrac" because need to go
		// into integral arithmetic:
		//
		double utc   = (m_TAI - m_DeltaAT).MJD().Magnitude();
		long   mjd   = long(floor(utc));
		double dFrac = utc - double(mjd);
		assert(0 <= dFrac && dFrac < 1.0);

		// Check if we are just BEFORE a 1-sec step, i.e. in a leap sec, then the
		// "dFrac" will need to be modified:
		vector<DATNode>::const_iterator it =
			find_if
			(dats.begin(), dats.end(),
			 [&](DATNode const& right) -> bool
					{ return right.m_StepTAI > m_TAI && right.m_DriftRate == 0.0; }
					// NB: strictly ">", otherwise we are already after the step!..
			);
		if (it != dats.end())
		{
			// Yes, it's a step, see how far it is from now:
			TimeS x = To_Time_sec(it->m_StepTAI - m_TAI);
			assert(x > TimeS(0.0));
			if (x <= TimeS(1.0))
			{
				// "dFrac" is now by (1-x) sec over 0, where 0 <= 1-x < 1; need to get
				// into the previous day's "dFrac" which is over 1:
				--mjd;
				dFrac += 1.0;
			}
		}
		assert(0.0 <= dFrac && dFrac < 86401.0 / 86400.0);

		// Get the calendar date from the integral part ("mjd"):
		FromMJD(mjd, &m_Year, &m_Month, &m_Day);

		assert(1 <= m_Month && m_Month <= 12);
		assert(1 <= m_Day   && m_Day   <= MonthLength(m_Year, m_Month));

		// Get the time of day:
		m_hour = ::min<int>(int(floor(dFrac * 24.0)), 23);
		m_min  = ::min<int>(int(floor(dFrac * 1440.0)) - m_hour * 60, 59);
		m_sec  = dFrac * 86400.0 - double(m_hour) * 3600.0 - double(m_min) * 60.0;
		assert(0.0 <= m_sec && m_sec < 61.0);
	}

	//=========================================================================//
	// Coversions Between Uniform Scales:                                      //
	//=========================================================================//
	//-------------------------------------------------------------------------//
	// TT <-> TDB:                                                             //
	//-------------------------------------------------------------------------//
	// NB: the result of "TDBmTT" does not really depend on whether the arg is in
	// TDB or TT -- the difference is negligible:
	//
	TDB ToTDB(TT tt)
	{
		TimeD mjd0 = tt.MJD();
		return TDB(mjd0) + TDBmTT(mjd0);
	}

	TT  ToTT(TDB tdb)
	{
		TimeD mjd1 = tdb.MJD();
		return  TT(mjd1) - TDBmTT(mjd1);
	}

	//-------------------------------------------------------------------------//
	// TDB <-> TCB:                                                            //
	//-------------------------------------------------------------------------//
	TCB ToTCB(TDB tdb)
	{
		// Compute the difference TAI(TDB) - TAI1977; we do NOT perform full
		// conversion TDB->TAI as periodic terms will have negligibe effect;
		// assume that TDB ~ TT:
		//
		TimeD delta(ToTAI(TT(tdb.MJD())) - TAI1977);
		return  TCB(tdb.MJD() + LB * delta);
	}

	TDB ToTDB(TCB tcb)
	{
		// Again, to compute TAI(MJD) - TAI(1977 Jan 1.0). However, we CANNOT
		// directly assume that TCB ~ TT as there is a drift of ~0.5 sec/year
		// between them, so must do a linear solution. We get:
		//
		// TCB  = TDB + LB * delta ~= TT + LB * delta = TT + LB * (TAI - TAI1977)
		//      = TT  + LB * (TT - 32.184s - TAI1977)
		// and therefore
		// TCB ~= TT * (1 + LB) - LB * (32.184s + TAI1977),
		// TT  ~= (TCB + LB * (32.184s + TAI1977)) / (1 + LB),
		// this value of TT is accurate enough to compute delta and then TDB:
		//
		TT tt((tcb.MJD() + LB * (To_Time_day(TimeS(32.184)) + TAI1977.MJD())) /
					(1.0 + LB));
		TimeD delta(ToTAI(tt) - TAI1977);
		return TDB(tcb.MJD()  - LB * delta);
	}

	//-------------------------------------------------------------------------//
	// TT <-> TCG:                                                             //
	//-------------------------------------------------------------------------//
	TCG ToTCG(TT tt)
	{
		// "delta" is now computed exactly:
		TimeD delta(ToTAI(tt) - TAI1977);
		return  TCG(tt.MJD()  + LG * delta);
	}

	TT ToTT(TCG tcg)
	{
		// Exact linear solution is available here. NB: Use LG, not LB!!!
		return TT((tcg.MJD() + LG * (To_Time_day(TimeS(32.184)) + TAI1977.MJD())) /
							(1.0 + LG));
	}
}
